<?php
// created: 2019-03-13 23:43:48
$mod_strings = array (
  'LBL_ROLE' => 'Rol',
  'LBL_NAME' => 'Nombre',
  'LBL_DESCRIPTION' => 'Descripción',
  'LIST_ROLES' => 'Lista de roles',
  'LBL_USERS_SUBPANEL_TITLE' => 'Usuarios',
  'LIST_ROLES_BY_USER' => 'Listar roles por usuarios',
  'LBL_ROLES_SUBPANEL_TITLE' => 'Roles de usuario',
  'LBL_SEARCH_FORM_TITLE' => 'Búsqueda',
  'LBL_NO_ACCESS' => 'No tiene acceso a esta área. Contacte con el administrador de su sitio web para obtenerlo',
  'LBL_REDIRECT_TO_HOME' => 'Redirigir a la página principal en',
  'LBL_SECONDS' => 'segundos',
  'LBL_ADDING' => 'Agregando para',
);