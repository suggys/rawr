

<script language="javascript">
    {literal}
    SUGAR.util.doWhen(function () {
        return $("#contentTable").length == 0;
    }, SUGAR.themes.actionMenu);
    {/literal}
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="{$module}">
<input type="hidden" name="record" value="{$fields.id.value}">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="{$offset}">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
{if !$config.enable_action_menu}
<div class="buttons">
{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AWR_Ordenes'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} 
{if $bean->aclAccess("edit")}<input title="{$APP.LBL_DUPLICATE_BUTTON_TITLE}" accessKey="{$APP.LBL_DUPLICATE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AWR_Ordenes'; _form.return_action.value='DetailView'; _form.isDuplicate.value=true; _form.action.value='EditView'; _form.return_id.value='{$id}';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Duplicate" value="{$APP.LBL_DUPLICATE_BUTTON_LABEL}" id="duplicate_button">{/if} 
{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AWR_Ordenes'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} 
{if $bean->aclAccess("edit") && $bean->aclAccess("delete")}<input title="{$APP.LBL_DUP_MERGE}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AWR_Ordenes'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='Step1'; _form.module.value='MergeRecords';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Merge" value="{$APP.LBL_DUP_MERGE}" id="merge_duplicate_button">{/if} 
<input type="button" class="button" onClick="showPopup('pdf');" value="{$MOD.LBL_PRINT_AS_PDF}"/>
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AWR_Ordenes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>                    {/if}
</form>
</div>
</td>
<td align="right" width="20%" class="buttons">{$ADMIN_EDIT}
</td>
</tr>
</table>
{sugar_include include=$includes}
<div class="detail-view">
<div class="mobile-pagination">{$PAGINATION}</div>

<ul class="nav nav-tabs">

{if $config.enable_action_menu and $config.enable_action_menu != false}
<li role="presentation" class="active">
<a id="tab0" data-toggle="tab" class="hidden-xs">
{sugar_translate label='DEFAULT' module='AWR_Ordenes'}
</a>
<a id="xstab0" href="#" class="visible-xs first-tab dropdown-toggle" data-toggle="dropdown">
{sugar_translate label='DEFAULT' module='AWR_Ordenes'}
</a>
</li>









{/if}
{if $config.enable_action_menu and $config.enable_action_menu != false}
<li id="tab-actions" class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">Acciones<span class="suitepicon suitepicon-action-caret"></span></a>
<ul class="dropdown-menu">
<li>{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AWR_Ordenes'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} </li>
<li>{if $bean->aclAccess("edit")}<input title="{$APP.LBL_DUPLICATE_BUTTON_TITLE}" accessKey="{$APP.LBL_DUPLICATE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AWR_Ordenes'; _form.return_action.value='DetailView'; _form.isDuplicate.value=true; _form.action.value='EditView'; _form.return_id.value='{$id}';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Duplicate" value="{$APP.LBL_DUPLICATE_BUTTON_LABEL}" id="duplicate_button">{/if} </li>
<li>{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AWR_Ordenes'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} </li>
<li>{if $bean->aclAccess("edit") && $bean->aclAccess("delete")}<input title="{$APP.LBL_DUP_MERGE}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AWR_Ordenes'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='Step1'; _form.module.value='MergeRecords';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Merge" value="{$APP.LBL_DUP_MERGE}" id="merge_duplicate_button">{/if} </li>
<li><input type="button" class="button" onClick="showPopup('pdf');" value="{$MOD.LBL_PRINT_AS_PDF}"/></li>
<li>{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AWR_Ordenes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}</li>
</ul>        </li>
<li class="tab-inline-pagination">
{$PAGINATION}
</li>
{/if}
</ul>
<div class="clearfix"></div>

{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="tab-content">
{else}

<div class="tab-content" style="padding: 0; border: 0;">
{/if}


{if $config.enable_action_menu and $config.enable_action_menu != false}
<div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-0'>





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ESTATUS_ORDEN' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="estatus_orden"  >

{if !$fields.estatus_orden.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.estatus_orden.options)}
<input type="hidden" class="sugar_field" id="{$fields.estatus_orden.name}" value="{ $fields.estatus_orden.options }">
{ $fields.estatus_orden.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.estatus_orden.name}" value="{ $fields.estatus_orden.value }">
{ $fields.estatus_orden.options[$fields.estatus_orden.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_NAME' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="name" field="name"  >

{if !$fields.name.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.name.value) <= 0}
{assign var="value" value=$fields.name.default_value }
{else}
{assign var="value" value=$fields.name.value }
{/if} 
<span class="sugar_field" id="{$fields.name.name}">{$fields.name.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ASESOR' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="asesor"  >

{if !$fields.asesor.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.asesor.value) <= 0}
{assign var="value" value=$fields.asesor.default_value }
{else}
{assign var="value" value=$fields.asesor.value }
{/if} 
<span class="sugar_field" id="{$fields.asesor.name}">{$fields.asesor.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ASIGNACION' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="asignacion"  >

{if !$fields.asignacion.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.awr_asignaciones_id_c.value)}
{capture assign="detail_url"}index.php?module=AWR_Asignaciones&action=DetailView&record={$fields.awr_asignaciones_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="awr_asignaciones_id_c" class="sugar_field" data-id-value="{$fields.awr_asignaciones_id_c.value}">{$fields.asignacion.value}</span>
{if !empty($fields.awr_asignaciones_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_NUMERO_SINIESTRO_TXT' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="numero_siniestro_txt"  >

{if !$fields.numero_siniestro_txt.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.numero_siniestro_txt.value) <= 0}
{assign var="value" value=$fields.numero_siniestro_txt.default_value }
{else}
{assign var="value" value=$fields.numero_siniestro_txt.value }
{/if} 
<span class="sugar_field" id="{$fields.numero_siniestro_txt.name}">{$fields.numero_siniestro_txt.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MARCA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="marca"  >

{if !$fields.marca.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.marca.value) <= 0}
{assign var="value" value=$fields.marca.default_value }
{else}
{assign var="value" value=$fields.marca.value }
{/if} 
<span class="sugar_field" id="{$fields.marca.name}">{$fields.marca.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MODELO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="modelo"  >

{if !$fields.modelo.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.modelo.value) <= 0}
{assign var="value" value=$fields.modelo.default_value }
{else}
{assign var="value" value=$fields.modelo.value }
{/if} 
<span class="sugar_field" id="{$fields.modelo.name}">{$fields.modelo.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ANIO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="anio"  >

{if !$fields.anio.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.anio.value) <= 0}
{assign var="value" value=$fields.anio.default_value }
{else}
{assign var="value" value=$fields.anio.value }
{/if} 
<span class="sugar_field" id="{$fields.anio.name}">{$fields.anio.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_NUM_SERIE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="num_serie"  >

{if !$fields.num_serie.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.num_serie.value) <= 0}
{assign var="value" value=$fields.num_serie.default_value }
{else}
{assign var="value" value=$fields.num_serie.value }
{/if} 
<span class="sugar_field" id="{$fields.num_serie.name}">{$fields.num_serie.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                        </div>
{else}

<div class="tab-pane-NOBOOTSTRAPTOGGLER panel-collapse"></div>
{/if}
</div>

<div class="panel-content">
<div>&nbsp;</div>





{if $config.enable_action_menu and $config.enable_action_menu != false}

{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel--1" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='DEFAULT' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel--1" data-id="DEFAULT">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ESTATUS_ORDEN' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="estatus_orden"  >

{if !$fields.estatus_orden.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.estatus_orden.options)}
<input type="hidden" class="sugar_field" id="{$fields.estatus_orden.name}" value="{ $fields.estatus_orden.options }">
{ $fields.estatus_orden.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.estatus_orden.name}" value="{ $fields.estatus_orden.value }">
{ $fields.estatus_orden.options[$fields.estatus_orden.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_NAME' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="name" field="name"  >

{if !$fields.name.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.name.value) <= 0}
{assign var="value" value=$fields.name.default_value }
{else}
{assign var="value" value=$fields.name.value }
{/if} 
<span class="sugar_field" id="{$fields.name.name}">{$fields.name.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ASESOR' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="asesor"  >

{if !$fields.asesor.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.asesor.value) <= 0}
{assign var="value" value=$fields.asesor.default_value }
{else}
{assign var="value" value=$fields.asesor.value }
{/if} 
<span class="sugar_field" id="{$fields.asesor.name}">{$fields.asesor.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ASIGNACION' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="asignacion"  >

{if !$fields.asignacion.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.awr_asignaciones_id_c.value)}
{capture assign="detail_url"}index.php?module=AWR_Asignaciones&action=DetailView&record={$fields.awr_asignaciones_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="awr_asignaciones_id_c" class="sugar_field" data-id-value="{$fields.awr_asignaciones_id_c.value}">{$fields.asignacion.value}</span>
{if !empty($fields.awr_asignaciones_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_NUMERO_SINIESTRO_TXT' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="numero_siniestro_txt"  >

{if !$fields.numero_siniestro_txt.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.numero_siniestro_txt.value) <= 0}
{assign var="value" value=$fields.numero_siniestro_txt.default_value }
{else}
{assign var="value" value=$fields.numero_siniestro_txt.value }
{/if} 
<span class="sugar_field" id="{$fields.numero_siniestro_txt.name}">{$fields.numero_siniestro_txt.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MARCA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="marca"  >

{if !$fields.marca.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.marca.value) <= 0}
{assign var="value" value=$fields.marca.default_value }
{else}
{assign var="value" value=$fields.marca.value }
{/if} 
<span class="sugar_field" id="{$fields.marca.name}">{$fields.marca.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MODELO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="modelo"  >

{if !$fields.modelo.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.modelo.value) <= 0}
{assign var="value" value=$fields.modelo.default_value }
{else}
{assign var="value" value=$fields.modelo.value }
{/if} 
<span class="sugar_field" id="{$fields.modelo.name}">{$fields.modelo.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ANIO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="anio"  >

{if !$fields.anio.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.anio.value) <= 0}
{assign var="value" value=$fields.anio.default_value }
{else}
{assign var="value" value=$fields.anio.value }
{/if} 
<span class="sugar_field" id="{$fields.anio.name}">{$fields.anio.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_NUM_SERIE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="num_serie"  >

{if !$fields.num_serie.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.num_serie.value) <= 0}
{assign var="value" value=$fields.num_serie.default_value }
{else}
{assign var="value" value=$fields.num_serie.value }
{/if} 
<span class="sugar_field" id="{$fields.num_serie.name}">{$fields.num_serie.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-0" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-0"  data-id="LBL_EDITVIEW_PANEL1">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_COSMETICA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="cosmetica"  >

{if !$fields.cosmetica.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.cosmetica.value) == "1" || strval($fields.cosmetica.value) == "yes" || strval($fields.cosmetica.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.cosmetica.name}" id="{$fields.cosmetica.name}" value="$fields.cosmetica.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_COS_ENDEREZADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="cos_enderezado"  >

{if !$fields.cos_enderezado.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.cos_enderezado.value) == "1" || strval($fields.cos_enderezado.value) == "yes" || strval($fields.cos_enderezado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.cos_enderezado.name}" id="{$fields.cos_enderezado.name}" value="$fields.cos_enderezado.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_REEMPLAZO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="reemplazo"  >

{if !$fields.reemplazo.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.reemplazo.value) == "1" || strval($fields.reemplazo.value) == "yes" || strval($fields.reemplazo.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.reemplazo.name}" id="{$fields.reemplazo.name}" value="$fields.reemplazo.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_FISURA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="fisura"  >

{if !$fields.fisura.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.fisura.value) == "1" || strval($fields.fisura.value) == "yes" || strval($fields.fisura.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.fisura.name}" id="{$fields.fisura.name}" value="$fields.fisura.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ENDEREZADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="enderezado"  >

{if !$fields.enderezado.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.enderezado.value) == "1" || strval($fields.enderezado.value) == "yes" || strval($fields.enderezado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.enderezado.name}" id="{$fields.enderezado.name}" value="$fields.enderezado.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PERDIDA_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="perdida_metal"  >

{if !$fields.perdida_metal.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.perdida_metal.value) == "1" || strval($fields.perdida_metal.value) == "yes" || strval($fields.perdida_metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.perdida_metal.name}" id="{$fields.perdida_metal.name}" value="$fields.perdida_metal.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_REMANUFACTURA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="remanufactura"  >

{if !$fields.remanufactura.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.remanufactura.value) == "1" || strval($fields.remanufactura.value) == "yes" || strval($fields.remanufactura.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.remanufactura.name}" id="{$fields.remanufactura.name}" value="$fields.remanufactura.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_OTRO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="otro"  >

{if !$fields.otro.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.otro.value) <= 0}
{assign var="value" value=$fields.otro.default_value }
{else}
{assign var="value" value=$fields.otro.value }
{/if} 
<span class="sugar_field" id="{$fields.otro.name}">{$fields.otro.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DANIO_DG' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="danio_dg"  >

{if !$fields.danio_dg.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.danio_dg.value) == "1" || strval($fields.danio_dg.value) == "yes" || strval($fields.danio_dg.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.danio_dg.name}" id="{$fields.danio_dg.name}" value="$fields.danio_dg.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DANIO_DL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="danio_dl"  >

{if !$fields.danio_dl.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.danio_dl.value) == "1" || strval($fields.danio_dl.value) == "yes" || strval($fields.danio_dl.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.danio_dl.name}" id="{$fields.danio_dl.name}" value="$fields.danio_dl.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-0" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-0" data-id="LBL_EDITVIEW_PANEL1">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_COSMETICA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="cosmetica"  >

{if !$fields.cosmetica.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.cosmetica.value) == "1" || strval($fields.cosmetica.value) == "yes" || strval($fields.cosmetica.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.cosmetica.name}" id="{$fields.cosmetica.name}" value="$fields.cosmetica.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_COS_ENDEREZADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="cos_enderezado"  >

{if !$fields.cos_enderezado.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.cos_enderezado.value) == "1" || strval($fields.cos_enderezado.value) == "yes" || strval($fields.cos_enderezado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.cos_enderezado.name}" id="{$fields.cos_enderezado.name}" value="$fields.cos_enderezado.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_REEMPLAZO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="reemplazo"  >

{if !$fields.reemplazo.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.reemplazo.value) == "1" || strval($fields.reemplazo.value) == "yes" || strval($fields.reemplazo.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.reemplazo.name}" id="{$fields.reemplazo.name}" value="$fields.reemplazo.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_FISURA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="fisura"  >

{if !$fields.fisura.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.fisura.value) == "1" || strval($fields.fisura.value) == "yes" || strval($fields.fisura.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.fisura.name}" id="{$fields.fisura.name}" value="$fields.fisura.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ENDEREZADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="enderezado"  >

{if !$fields.enderezado.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.enderezado.value) == "1" || strval($fields.enderezado.value) == "yes" || strval($fields.enderezado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.enderezado.name}" id="{$fields.enderezado.name}" value="$fields.enderezado.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PERDIDA_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="perdida_metal"  >

{if !$fields.perdida_metal.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.perdida_metal.value) == "1" || strval($fields.perdida_metal.value) == "yes" || strval($fields.perdida_metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.perdida_metal.name}" id="{$fields.perdida_metal.name}" value="$fields.perdida_metal.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_REMANUFACTURA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="remanufactura"  >

{if !$fields.remanufactura.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.remanufactura.value) == "1" || strval($fields.remanufactura.value) == "yes" || strval($fields.remanufactura.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.remanufactura.name}" id="{$fields.remanufactura.name}" value="$fields.remanufactura.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_OTRO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="otro"  >

{if !$fields.otro.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.otro.value) <= 0}
{assign var="value" value=$fields.otro.default_value }
{else}
{assign var="value" value=$fields.otro.value }
{/if} 
<span class="sugar_field" id="{$fields.otro.name}">{$fields.otro.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DANIO_DG' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="danio_dg"  >

{if !$fields.danio_dg.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.danio_dg.value) == "1" || strval($fields.danio_dg.value) == "yes" || strval($fields.danio_dg.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.danio_dg.name}" id="{$fields.danio_dg.name}" value="$fields.danio_dg.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DANIO_DL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="danio_dl"  >

{if !$fields.danio_dl.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.danio_dl.value) == "1" || strval($fields.danio_dl.value) == "yes" || strval($fields.danio_dl.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.danio_dl.name}" id="{$fields.danio_dl.name}" value="$fields.danio_dl.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-1" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL2' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-1"  data-id="LBL_EDITVIEW_PANEL2">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DEL_DER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="del_der"  >

{if !$fields.del_der.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.del_der.value) == "1" || strval($fields.del_der.value) == "yes" || strval($fields.del_der.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.del_der.name}" id="{$fields.del_der.name}" value="$fields.del_der.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DEL_IZQ' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="del_izq"  >

{if !$fields.del_izq.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.del_izq.value) == "1" || strval($fields.del_izq.value) == "yes" || strval($fields.del_izq.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.del_izq.name}" id="{$fields.del_izq.name}" value="$fields.del_izq.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TRAS_DER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="tras_der"  >

{if !$fields.tras_der.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.tras_der.value) == "1" || strval($fields.tras_der.value) == "yes" || strval($fields.tras_der.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.tras_der.name}" id="{$fields.tras_der.name}" value="$fields.tras_der.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TRAS_IZQ' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="tras_izq"  >

{if !$fields.tras_izq.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.tras_izq.value) == "1" || strval($fields.tras_izq.value) == "yes" || strval($fields.tras_izq.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.tras_izq.name}" id="{$fields.tras_izq.name}" value="$fields.tras_izq.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-1" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL2' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-1" data-id="LBL_EDITVIEW_PANEL2">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DEL_DER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="del_der"  >

{if !$fields.del_der.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.del_der.value) == "1" || strval($fields.del_der.value) == "yes" || strval($fields.del_der.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.del_der.name}" id="{$fields.del_der.name}" value="$fields.del_der.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DEL_IZQ' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="del_izq"  >

{if !$fields.del_izq.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.del_izq.value) == "1" || strval($fields.del_izq.value) == "yes" || strval($fields.del_izq.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.del_izq.name}" id="{$fields.del_izq.name}" value="$fields.del_izq.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TRAS_DER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="tras_der"  >

{if !$fields.tras_der.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.tras_der.value) == "1" || strval($fields.tras_der.value) == "yes" || strval($fields.tras_der.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.tras_der.name}" id="{$fields.tras_der.name}" value="$fields.tras_der.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TRAS_IZQ' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="tras_izq"  >

{if !$fields.tras_izq.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.tras_izq.value) == "1" || strval($fields.tras_izq.value) == "yes" || strval($fields.tras_izq.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.tras_izq.name}" id="{$fields.tras_izq.name}" value="$fields.tras_izq.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-2" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL3' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-2"  data-id="LBL_EDITVIEW_PANEL3">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_HOLLANDER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="hollander"  >

{if !$fields.hollander.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.hollander.value) <= 0}
{assign var="value" value=$fields.hollander.default_value }
{else}
{assign var="value" value=$fields.hollander.value }
{/if} 
<span class="sugar_field" id="{$fields.hollander.name}">{$fields.hollander.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_HOL_CANTIDAD' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="int" field="hol_cantidad"  >

{if !$fields.hol_cantidad.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.hol_cantidad.name}">
{sugar_number_format precision=0 var=$fields.hol_cantidad.value}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_SIZE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="rin_size"  >

{if !$fields.rin_size.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.rin_size.value) <= 0}
{assign var="value" value=$fields.rin_size.default_value }
{else}
{assign var="value" value=$fields.rin_size.value }
{/if} 
<span class="sugar_field" id="{$fields.rin_size.name}">{$fields.rin_size.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-2" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL3' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-2" data-id="LBL_EDITVIEW_PANEL3">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_HOLLANDER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="hollander"  >

{if !$fields.hollander.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.hollander.value) <= 0}
{assign var="value" value=$fields.hollander.default_value }
{else}
{assign var="value" value=$fields.hollander.value }
{/if} 
<span class="sugar_field" id="{$fields.hollander.name}">{$fields.hollander.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_HOL_CANTIDAD' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="int" field="hol_cantidad"  >

{if !$fields.hol_cantidad.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.hol_cantidad.name}">
{sugar_number_format precision=0 var=$fields.hol_cantidad.value}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_SIZE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="rin_size"  >

{if !$fields.rin_size.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.rin_size.value) <= 0}
{assign var="value" value=$fields.rin_size.default_value }
{else}
{assign var="value" value=$fields.rin_size.value }
{/if} 
<span class="sugar_field" id="{$fields.rin_size.name}">{$fields.rin_size.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-3" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL4' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-3"  data-id="LBL_EDITVIEW_PANEL4">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ALUMINIO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="aluminio"  >

{if !$fields.aluminio.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.aluminio.value) == "1" || strval($fields.aluminio.value) == "yes" || strval($fields.aluminio.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.aluminio.name}" id="{$fields.aluminio.name}" value="$fields.aluminio.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ACERO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="acero"  >

{if !$fields.acero.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.acero.value) == "1" || strval($fields.acero.value) == "yes" || strval($fields.acero.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.acero.name}" id="{$fields.acero.name}" value="$fields.acero.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CROMADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="cromado"  >

{if !$fields.cromado.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.cromado.value) == "1" || strval($fields.cromado.value) == "yes" || strval($fields.cromado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.cromado.name}" id="{$fields.cromado.name}" value="$fields.cromado.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PULIDO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="pulido"  >

{if !$fields.pulido.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.pulido.value) == "1" || strval($fields.pulido.value) == "yes" || strval($fields.pulido.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.pulido.name}" id="{$fields.pulido.name}" value="$fields.pulido.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DIAMANTADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="diamantado"  >

{if !$fields.diamantado.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.diamantado.value) == "1" || strval($fields.diamantado.value) == "yes" || strval($fields.diamantado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.diamantado.name}" id="{$fields.diamantado.name}" value="$fields.diamantado.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PINTADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="pintado"  >

{if !$fields.pintado.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.pintado.value) == "1" || strval($fields.pintado.value) == "yes" || strval($fields.pintado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.pintado.name}" id="{$fields.pintado.name}" value="$fields.pintado.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_HYPER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="hyper"  >

{if !$fields.hyper.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.hyper.value) == "1" || strval($fields.hyper.value) == "yes" || strval($fields.hyper.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.hyper.name}" id="{$fields.hyper.name}" value="$fields.hyper.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CENTRO_RIN' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="centro_rin"  >

{if !$fields.centro_rin.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.centro_rin.value) == "1" || strval($fields.centro_rin.value) == "yes" || strval($fields.centro_rin.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.centro_rin.name}" id="{$fields.centro_rin.name}" value="$fields.centro_rin.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-3" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL4' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-3" data-id="LBL_EDITVIEW_PANEL4">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ALUMINIO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="aluminio"  >

{if !$fields.aluminio.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.aluminio.value) == "1" || strval($fields.aluminio.value) == "yes" || strval($fields.aluminio.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.aluminio.name}" id="{$fields.aluminio.name}" value="$fields.aluminio.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ACERO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="acero"  >

{if !$fields.acero.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.acero.value) == "1" || strval($fields.acero.value) == "yes" || strval($fields.acero.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.acero.name}" id="{$fields.acero.name}" value="$fields.acero.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CROMADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="cromado"  >

{if !$fields.cromado.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.cromado.value) == "1" || strval($fields.cromado.value) == "yes" || strval($fields.cromado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.cromado.name}" id="{$fields.cromado.name}" value="$fields.cromado.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PULIDO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="pulido"  >

{if !$fields.pulido.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.pulido.value) == "1" || strval($fields.pulido.value) == "yes" || strval($fields.pulido.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.pulido.name}" id="{$fields.pulido.name}" value="$fields.pulido.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DIAMANTADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="diamantado"  >

{if !$fields.diamantado.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.diamantado.value) == "1" || strval($fields.diamantado.value) == "yes" || strval($fields.diamantado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.diamantado.name}" id="{$fields.diamantado.name}" value="$fields.diamantado.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PINTADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="pintado"  >

{if !$fields.pintado.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.pintado.value) == "1" || strval($fields.pintado.value) == "yes" || strval($fields.pintado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.pintado.name}" id="{$fields.pintado.name}" value="$fields.pintado.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_HYPER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="hyper"  >

{if !$fields.hyper.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.hyper.value) == "1" || strval($fields.hyper.value) == "yes" || strval($fields.hyper.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.hyper.name}" id="{$fields.hyper.name}" value="$fields.hyper.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CENTRO_RIN' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="centro_rin"  >

{if !$fields.centro_rin.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.centro_rin.value) == "1" || strval($fields.centro_rin.value) == "yes" || strval($fields.centro_rin.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.centro_rin.name}" id="{$fields.centro_rin.name}" value="$fields.centro_rin.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-4" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL5' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-4"  data-id="LBL_EDITVIEW_PANEL5">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="metal"  >

{if !$fields.metal.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.metal.value) == "1" || strval($fields.metal.value) == "yes" || strval($fields.metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.metal.name}" id="{$fields.metal.name}" value="$fields.metal.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_HULE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="hule"  >

{if !$fields.hule.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.hule.value) == "1" || strval($fields.hule.value) == "yes" || strval($fields.hule.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.hule.name}" id="{$fields.hule.name}" value="$fields.hule.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CROMO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="cromo"  >

{if !$fields.cromo.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.cromo.value) == "1" || strval($fields.cromo.value) == "yes" || strval($fields.cromo.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.cromo.name}" id="{$fields.cromo.name}" value="$fields.cromo.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TAPON_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="tapon_metal"  >

{if !$fields.tapon_metal.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.tapon_metal.value) == "1" || strval($fields.tapon_metal.value) == "yes" || strval($fields.tapon_metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.tapon_metal.name}" id="{$fields.tapon_metal.name}" value="$fields.tapon_metal.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SENSOR' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="sensor"  >

{if !$fields.sensor.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.sensor.value) == "1" || strval($fields.sensor.value) == "yes" || strval($fields.sensor.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.sensor.name}" id="{$fields.sensor.name}" value="$fields.sensor.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TAPON_PIVOTE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="tapon_pivote"  >

{if !$fields.tapon_pivote.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.tapon_pivote.value) <= 0}
{assign var="value" value=$fields.tapon_pivote.default_value }
{else}
{assign var="value" value=$fields.tapon_pivote.value }
{/if} 
<span class="sugar_field" id="{$fields.tapon_pivote.name}">{$fields.tapon_pivote.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_COMENTARIOS' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="text" field="comentarios"  >

{if !$fields.comentarios.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.comentarios.name|escape:'html'|url2html|nl2br}">{$fields.comentarios.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-4" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL5' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-4" data-id="LBL_EDITVIEW_PANEL5">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="metal"  >

{if !$fields.metal.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.metal.value) == "1" || strval($fields.metal.value) == "yes" || strval($fields.metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.metal.name}" id="{$fields.metal.name}" value="$fields.metal.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_HULE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="hule"  >

{if !$fields.hule.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.hule.value) == "1" || strval($fields.hule.value) == "yes" || strval($fields.hule.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.hule.name}" id="{$fields.hule.name}" value="$fields.hule.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CROMO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="cromo"  >

{if !$fields.cromo.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.cromo.value) == "1" || strval($fields.cromo.value) == "yes" || strval($fields.cromo.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.cromo.name}" id="{$fields.cromo.name}" value="$fields.cromo.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TAPON_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="tapon_metal"  >

{if !$fields.tapon_metal.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.tapon_metal.value) == "1" || strval($fields.tapon_metal.value) == "yes" || strval($fields.tapon_metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.tapon_metal.name}" id="{$fields.tapon_metal.name}" value="$fields.tapon_metal.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SENSOR' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="sensor"  >

{if !$fields.sensor.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.sensor.value) == "1" || strval($fields.sensor.value) == "yes" || strval($fields.sensor.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.sensor.name}" id="{$fields.sensor.name}" value="$fields.sensor.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TAPON_PIVOTE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="tapon_pivote"  >

{if !$fields.tapon_pivote.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.tapon_pivote.value) <= 0}
{assign var="value" value=$fields.tapon_pivote.default_value }
{else}
{assign var="value" value=$fields.tapon_pivote.value }
{/if} 
<span class="sugar_field" id="{$fields.tapon_pivote.name}">{$fields.tapon_pivote.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_COMENTARIOS' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="text" field="comentarios"  >

{if !$fields.comentarios.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.comentarios.name|escape:'html'|url2html|nl2br}">{$fields.comentarios.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-5" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL6' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-5"  data-id="LBL_EDITVIEW_PANEL6">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRES_RDI_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="pres_rdi_num"  >

{if !$fields.pres_rdi_num.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.pres_rdi_num.name}'>
{sugar_number_format var=$fields.pres_rdi_num.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRES_RDD_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="pres_rdd_num"  >

{if !$fields.pres_rdd_num.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.pres_rdd_num.name}'>
{sugar_number_format var=$fields.pres_rdd_num.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRES_RTI_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="pres_rti_num"  >

{if !$fields.pres_rti_num.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.pres_rti_num.name}'>
{sugar_number_format var=$fields.pres_rti_num.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRED_RTD_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="pred_rtd_num"  >

{if !$fields.pred_rtd_num.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.pred_rtd_num.name}'>
{sugar_number_format var=$fields.pred_rtd_num.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TOTAL_RIN_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="total_rin_num"  >

{if !$fields.total_rin_num.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.total_rin_num.name}'>
{sugar_number_format var=$fields.total_rin_num.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-5" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL6' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-5" data-id="LBL_EDITVIEW_PANEL6">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRES_RDI_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="pres_rdi_num"  >

{if !$fields.pres_rdi_num.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.pres_rdi_num.name}'>
{sugar_number_format var=$fields.pres_rdi_num.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRES_RDD_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="pres_rdd_num"  >

{if !$fields.pres_rdd_num.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.pres_rdd_num.name}'>
{sugar_number_format var=$fields.pres_rdd_num.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRES_RTI_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="pres_rti_num"  >

{if !$fields.pres_rti_num.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.pres_rti_num.name}'>
{sugar_number_format var=$fields.pres_rti_num.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRED_RTD_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="pred_rtd_num"  >

{if !$fields.pred_rtd_num.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.pred_rtd_num.name}'>
{sugar_number_format var=$fields.pred_rtd_num.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TOTAL_RIN_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="total_rin_num"  >

{if !$fields.total_rin_num.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.total_rin_num.name}'>
{sugar_number_format var=$fields.total_rin_num.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-6" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL7' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-6"  data-id="LBL_EDITVIEW_PANEL7">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MONTAR_USADA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="montar_usada"  >

{if !$fields.montar_usada.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.montar_usada.value) == "1" || strval($fields.montar_usada.value) == "yes" || strval($fields.montar_usada.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.montar_usada.name}" id="{$fields.montar_usada.name}" value="$fields.montar_usada.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MONTAR_NUEVA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="montar_nueva"  >

{if !$fields.montar_nueva.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.montar_nueva.value) == "1" || strval($fields.montar_nueva.value) == "yes" || strval($fields.montar_nueva.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.montar_nueva.name}" id="{$fields.montar_nueva.name}" value="$fields.montar_nueva.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INSTR_DESCR_LLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="instr_descr_llanta"  >

{if !$fields.instr_descr_llanta.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.instr_descr_llanta.value) <= 0}
{assign var="value" value=$fields.instr_descr_llanta.default_value }
{else}
{assign var="value" value=$fields.instr_descr_llanta.value }
{/if} 
<span class="sugar_field" id="{$fields.instr_descr_llanta.name}">{$fields.instr_descr_llanta.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INSTR_TAMANIO_LLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="instr_tamanio_llanta"  >

{if !$fields.instr_tamanio_llanta.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.instr_tamanio_llanta.value) <= 0}
{assign var="value" value=$fields.instr_tamanio_llanta.default_value }
{else}
{assign var="value" value=$fields.instr_tamanio_llanta.value }
{/if} 
<span class="sugar_field" id="{$fields.instr_tamanio_llanta.name}">{$fields.instr_tamanio_llanta.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-6" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL7' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-6" data-id="LBL_EDITVIEW_PANEL7">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MONTAR_USADA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="montar_usada"  >

{if !$fields.montar_usada.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.montar_usada.value) == "1" || strval($fields.montar_usada.value) == "yes" || strval($fields.montar_usada.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.montar_usada.name}" id="{$fields.montar_usada.name}" value="$fields.montar_usada.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MONTAR_NUEVA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="montar_nueva"  >

{if !$fields.montar_nueva.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.montar_nueva.value) == "1" || strval($fields.montar_nueva.value) == "yes" || strval($fields.montar_nueva.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.montar_nueva.name}" id="{$fields.montar_nueva.name}" value="$fields.montar_nueva.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INSTR_DESCR_LLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="instr_descr_llanta"  >

{if !$fields.instr_descr_llanta.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.instr_descr_llanta.value) <= 0}
{assign var="value" value=$fields.instr_descr_llanta.default_value }
{else}
{assign var="value" value=$fields.instr_descr_llanta.value }
{/if} 
<span class="sugar_field" id="{$fields.instr_descr_llanta.name}">{$fields.instr_descr_llanta.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INSTR_TAMANIO_LLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="instr_tamanio_llanta"  >

{if !$fields.instr_tamanio_llanta.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.instr_tamanio_llanta.value) <= 0}
{assign var="value" value=$fields.instr_tamanio_llanta.default_value }
{else}
{assign var="value" value=$fields.instr_tamanio_llanta.value }
{/if} 
<span class="sugar_field" id="{$fields.instr_tamanio_llanta.name}">{$fields.instr_tamanio_llanta.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-7" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL8' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-7"  data-id="LBL_EDITVIEW_PANEL8">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_UNIDAD_RECIBIDA_PLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="unidad_recibida_planta"  >

{if !$fields.unidad_recibida_planta.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.unidad_recibida_planta.value) == "1" || strval($fields.unidad_recibida_planta.value) == "yes" || strval($fields.unidad_recibida_planta.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.unidad_recibida_planta.name}" id="{$fields.unidad_recibida_planta.name}" value="$fields.unidad_recibida_planta.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_CON_LLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="rin_con_llanta"  >

{if !$fields.rin_con_llanta.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_con_llanta.value) == "1" || strval($fields.rin_con_llanta.value) == "yes" || strval($fields.rin_con_llanta.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.rin_con_llanta.name}" id="{$fields.rin_con_llanta.name}" value="$fields.rin_con_llanta.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_APORTE_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="rin_aporte_metal"  >

{if !$fields.rin_aporte_metal.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_aporte_metal.value) == "1" || strval($fields.rin_aporte_metal.value) == "yes" || strval($fields.rin_aporte_metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.rin_aporte_metal.name}" id="{$fields.rin_aporte_metal.name}" value="$fields.rin_aporte_metal.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_2_PIEZAS' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="rin_2_piezas"  >

{if !$fields.rin_2_piezas.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_2_piezas.value) == "1" || strval($fields.rin_2_piezas.value) == "yes" || strval($fields.rin_2_piezas.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.rin_2_piezas.name}" id="{$fields.rin_2_piezas.name}" value="$fields.rin_2_piezas.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_EMPAPELAR' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="rin_empapelar"  >

{if !$fields.rin_empapelar.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_empapelar.value) == "1" || strval($fields.rin_empapelar.value) == "yes" || strval($fields.rin_empapelar.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.rin_empapelar.name}" id="{$fields.rin_empapelar.name}" value="$fields.rin_empapelar.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_2_TONOS' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="rin_2_tonos"  >

{if !$fields.rin_2_tonos.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_2_tonos.value) == "1" || strval($fields.rin_2_tonos.value) == "yes" || strval($fields.rin_2_tonos.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.rin_2_tonos.name}" id="{$fields.rin_2_tonos.name}" value="$fields.rin_2_tonos.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TIPO_RIN' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="tipo_rin"  >

{if !$fields.tipo_rin.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.tipo_rin.options)}
<input type="hidden" class="sugar_field" id="{$fields.tipo_rin.name}" value="{ $fields.tipo_rin.options }">
{ $fields.tipo_rin.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.tipo_rin.name}" value="{ $fields.tipo_rin.value }">
{ $fields.tipo_rin.options[$fields.tipo_rin.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-7" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL8' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-7" data-id="LBL_EDITVIEW_PANEL8">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_UNIDAD_RECIBIDA_PLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="unidad_recibida_planta"  >

{if !$fields.unidad_recibida_planta.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.unidad_recibida_planta.value) == "1" || strval($fields.unidad_recibida_planta.value) == "yes" || strval($fields.unidad_recibida_planta.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.unidad_recibida_planta.name}" id="{$fields.unidad_recibida_planta.name}" value="$fields.unidad_recibida_planta.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_CON_LLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="rin_con_llanta"  >

{if !$fields.rin_con_llanta.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_con_llanta.value) == "1" || strval($fields.rin_con_llanta.value) == "yes" || strval($fields.rin_con_llanta.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.rin_con_llanta.name}" id="{$fields.rin_con_llanta.name}" value="$fields.rin_con_llanta.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_APORTE_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="rin_aporte_metal"  >

{if !$fields.rin_aporte_metal.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_aporte_metal.value) == "1" || strval($fields.rin_aporte_metal.value) == "yes" || strval($fields.rin_aporte_metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.rin_aporte_metal.name}" id="{$fields.rin_aporte_metal.name}" value="$fields.rin_aporte_metal.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_2_PIEZAS' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="rin_2_piezas"  >

{if !$fields.rin_2_piezas.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_2_piezas.value) == "1" || strval($fields.rin_2_piezas.value) == "yes" || strval($fields.rin_2_piezas.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.rin_2_piezas.name}" id="{$fields.rin_2_piezas.name}" value="$fields.rin_2_piezas.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_EMPAPELAR' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="rin_empapelar"  >

{if !$fields.rin_empapelar.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_empapelar.value) == "1" || strval($fields.rin_empapelar.value) == "yes" || strval($fields.rin_empapelar.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.rin_empapelar.name}" id="{$fields.rin_empapelar.name}" value="$fields.rin_empapelar.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_2_TONOS' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="rin_2_tonos"  >

{if !$fields.rin_2_tonos.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_2_tonos.value) == "1" || strval($fields.rin_2_tonos.value) == "yes" || strval($fields.rin_2_tonos.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="checkbox" class="checkbox" name="{$fields.rin_2_tonos.name}" id="{$fields.rin_2_tonos.name}" value="$fields.rin_2_tonos.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TIPO_RIN' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="tipo_rin"  >

{if !$fields.tipo_rin.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.tipo_rin.options)}
<input type="hidden" class="sugar_field" id="{$fields.tipo_rin.name}" value="{ $fields.tipo_rin.options }">
{ $fields.tipo_rin.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.tipo_rin.name}" value="{ $fields.tipo_rin.value }">
{ $fields.tipo_rin.options[$fields.tipo_rin.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-8" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL9' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-8"  data-id="LBL_EDITVIEW_PANEL9">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_IMG_FIRMA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="image" field="img_firma"  >

{if !$fields.img_firma.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.img_firma.name}">
{if strlen($fields.img_firma.value) <= 0}
<img src="" style="max-width: {if !$vardef.width}120{else}200{/if}px;" height="{if !$vardef.height}{else}50{/if}">
{else}
<img src="index.php?entryPoint=download&id={$fields.id.value}_{$fields.img_firma.name}{$fields.width.value}&type={$module}" style="max-width: {if !$vardef.width}120{else}200{/if}px;" height="{if !$vardef.height}{else}50{/if}">
{/if}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_IMG_ENT' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="image" field="img_ent"  >

{if !$fields.img_ent.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.img_ent.name}">
{if strlen($fields.img_ent.value) <= 0}
<img src="" style="max-width: {if !$vardef.width}120{else}200{/if}px;" height="{if !$vardef.height}{else}50{/if}">
{else}
<img src="index.php?entryPoint=download&id={$fields.id.value}_{$fields.img_ent.name}{$fields.width.value}&type={$module}" style="max-width: {if !$vardef.width}120{else}200{/if}px;" height="{if !$vardef.height}{else}50{/if}">
{/if}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-8" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL9' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-8" data-id="LBL_EDITVIEW_PANEL9">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_IMG_FIRMA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="image" field="img_firma"  >

{if !$fields.img_firma.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.img_firma.name}">
{if strlen($fields.img_firma.value) <= 0}
<img src="" style="max-width: {if !$vardef.width}120{else}200{/if}px;" height="{if !$vardef.height}{else}50{/if}">
{else}
<img src="index.php?entryPoint=download&id={$fields.id.value}_{$fields.img_firma.name}{$fields.width.value}&type={$module}" style="max-width: {if !$vardef.width}120{else}200{/if}px;" height="{if !$vardef.height}{else}50{/if}">
{/if}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>




<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_IMG_ENT' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="image" field="img_ent"  >

{if !$fields.img_ent.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.img_ent.name}">
{if strlen($fields.img_ent.value) <= 0}
<img src="" style="max-width: {if !$vardef.width}120{else}200{/if}px;" height="{if !$vardef.height}{else}50{/if}">
{else}
<img src="index.php?entryPoint=download&id={$fields.id.value}_{$fields.img_ent.name}{$fields.width.value}&type={$module}" style="max-width: {if !$vardef.width}120{else}200{/if}px;" height="{if !$vardef.height}{else}50{/if}">
{/if}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

</div>
                            </div>
</div>
</div>
{/if}
</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script>            <script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
{literal}
<script type="text/javascript">

                    var selectTabDetailView = function(tab) {
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').hide();
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').eq(tab).show().addClass('active').addClass('in');
                        $('#content div.panel-content div.panel').hide();
                        $('#content div.panel-content div.panel.tab-panel-' + tab).show();
                    };

                    var selectTabOnError = function(tab) {
                        selectTabDetailView(tab);
                        $('#content ul.nav.nav-tabs > li').removeClass('active');
                        $('#content ul.nav.nav-tabs > li a').css('color', '');

                        $('#content ul.nav.nav-tabs > li').eq(tab).find('a').first().css('color', 'red');
                        $('#content ul.nav.nav-tabs > li').eq(tab).addClass('active');

                    };

                    var selectTabOnErrorInputHandle = function(inputHandle) {
                        var tab = $(inputHandle).closest('.tab-pane-NOBOOTSTRAPTOGGLER').attr('id').match(/^detailpanel_(.*)$/)[1];
                        selectTabOnError(tab);
                    };


                    $(function(){
                        $('#content ul.nav.nav-tabs > li > a[data-toggle="tab"]').click(function(e){
                            if(typeof $(this).parent().find('a').first().attr('id') != 'undefined') {
                                var tab = parseInt($(this).parent().find('a').first().attr('id').match(/^tab(.)*$/)[1]);
                                selectTabDetailView(tab);
                            }
                        });
                    });

                </script>
{/literal}