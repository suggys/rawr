

<script>
    {literal}
    $(document).ready(function(){
	    $("ul.clickMenu").each(function(index, node){
	        $(node).sugarActionMenu();
	    });

        if($('.edit-view-pagination').children().length == 0) $('.saveAndContinue').remove();
    });
    {/literal}
</script>
<div class="clear"></div>
<form action="index.php" method="POST" name="{$form_name}" id="{$form_id}" {$enctype}>
<div class="edit-view-pagination-mobile-container">
<div class="edit-view-pagination edit-view-mobile-pagination">
{$PAGINATION}
</div>
</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit">
<tr>
<td class="buttons">
<input type="hidden" name="module" value="{$module}">
{if isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true"}
<input type="hidden" name="record" value="">
<input type="hidden" name="duplicateSave" value="true">
<input type="hidden" name="duplicateId" value="{$fields.id.value}">
{else}
<input type="hidden" name="record" value="{$fields.id.value}">
{/if}
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="action">
<input type="hidden" name="return_module" value="{$smarty.request.return_module}">
<input type="hidden" name="return_action" value="{$smarty.request.return_action}">
<input type="hidden" name="return_id" value="{$smarty.request.return_id}">
<input type="hidden" name="module_tab"> 
<input type="hidden" name="contact_role">
{if (!empty($smarty.request.return_module) || !empty($smarty.request.relate_to)) && !(isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true")}
<input type="hidden" name="relate_to" value="{if $smarty.request.return_relationship}{$smarty.request.return_relationship}{elseif $smarty.request.relate_to && empty($smarty.request.from_dcmenu)}{$smarty.request.relate_to}{elseif empty($isDCForm) && empty($smarty.request.from_dcmenu)}{$smarty.request.return_module}{/if}">
<input type="hidden" name="relate_id" value="{$smarty.request.return_id}">
{/if}
<input type="hidden" name="offset" value="{$offset}">
{assign var='place' value="_HEADER"} <!-- to be used for id for buttons with custom code in def files-->
<div class="buttons">
{if $bean->aclAccess("save")}<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('EditView'); {if $isDuplicate}_form.return_id.value=''; {/if}_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" id="SAVE">{/if} 
{if !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($smarty.request.return_id))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" type="button" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($fields.id.value))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && empty($fields.id.value)) && empty($smarty.request.return_id)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && !empty($smarty.request.return_module)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action={$smarty.request.return_action}&module={$smarty.request.return_module|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif empty($smarty.request.return_action) || empty($smarty.request.return_id) && !empty($fields.id.value)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=AWR_Ordenes'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {else}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {/if}
{if $showVCRControl}
<button type="button" id="save_and_continue" class="button saveAndContinue" title="{$app_strings.LBL_SAVE_AND_CONTINUE}" onClick="SUGAR.saveAndContinue(this);">
{$APP.LBL_SAVE_AND_CONTINUE}
</button>
{/if}
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AWR_Ordenes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>
</td>
<td align='right' class="edit-view-pagination-desktop-container">
<div class="edit-view-pagination edit-view-pagination-desktop">
{$PAGINATION}
</div>
</td>
</tr>
</table>
{sugar_include include=$includes}
<div id="EditView_tabs">

<ul class="nav nav-tabs">
</ul>
<div class="clearfix"></div>
<div class="tab-content" style="padding: 0; border: 0;">

<div class="tab-pane panel-collapse">&nbsp;</div>
</div>

<div class="panel-content">
<div>&nbsp;</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='DEFAULT' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_-1" data-id="DEFAULT">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_ESTATUS_ORDEN">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_ESTATUS_ORDEN' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="estatus_orden"  >
{counter name="panelFieldCount" print=false}

{if !isset($config.enable_autocomplete) || $config.enable_autocomplete==false}
<select name="{$fields.estatus_orden.name}" 
id="{$fields.estatus_orden.name}" 
title=''       
>
{if isset($fields.estatus_orden.value) && $fields.estatus_orden.value != ''}
{html_options options=$fields.estatus_orden.options selected=$fields.estatus_orden.value}
{else}
{html_options options=$fields.estatus_orden.options selected=$fields.estatus_orden.default}
{/if}
</select>
{else}
{assign var="field_options" value=$fields.estatus_orden.options }
{capture name="field_val"}{$fields.estatus_orden.value}{/capture}
{assign var="field_val" value=$smarty.capture.field_val}
{capture name="ac_key"}{$fields.estatus_orden.name}{/capture}
{assign var="ac_key" value=$smarty.capture.ac_key}
<select style='display:none' name="{$fields.estatus_orden.name}" 
id="{$fields.estatus_orden.name}" 
title=''          
>
{if isset($fields.estatus_orden.value) && $fields.estatus_orden.value != ''}
{html_options options=$fields.estatus_orden.options selected=$fields.estatus_orden.value}
{else}
{html_options options=$fields.estatus_orden.options selected=$fields.estatus_orden.default}
{/if}
</select>
<input
id="{$fields.estatus_orden.name}-input"
name="{$fields.estatus_orden.name}-input"
size="30"
value="{$field_val|lookup:$field_options}"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="{sugar_getimagepath file="id-ff-down.png"}" id="{$fields.estatus_orden.name}-image"></button><button type="button"
id="btn-clear-{$fields.estatus_orden.name}-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '{$fields.estatus_orden.name}-input', '{$fields.estatus_orden.name}');sync_{$fields.estatus_orden.name}()"><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
{literal}
<script>
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal} = [];
	{/literal}

			{literal}
		(function (){
			var selectElem = document.getElementById("{/literal}{$fields.estatus_orden.name}{literal}");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:''};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use('datasource', 'datasource-jsonschema',function (Y) {
				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value=='' && selectElem.options[i].innerHTML==''))
				    			ret.push({'key':selectElem.options[i].value,'text':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		{/literal}
	
	{literal}
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	{/literal}
			
	SUGAR.AutoComplete.{$ac_key}.inputNode = Y.one('#{$fields.estatus_orden.name}-input');
	SUGAR.AutoComplete.{$ac_key}.inputImage = Y.one('#{$fields.estatus_orden.name}-image');
	SUGAR.AutoComplete.{$ac_key}.inputHidden = Y.one('#{$fields.estatus_orden.name}');
	
			{literal}
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("{/literal}{$fields.estatus_orden.name}{literal}");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('change');
			}

			//global variable 
			sync_{/literal}{$fields.estatus_orden.name}{literal} = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("{/literal}{$fields.estatus_orden.name}{literal}");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value');

				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.simulate('keyup');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById('{/literal}{$fields.estatus_orden.name}-input{literal}'))
						SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("{/literal}{$fields.estatus_orden.name}{literal}", syncFromHiddenToWidget);
		{/literal}

		SUGAR.AutoComplete.{$ac_key}.minQLen = 0;
		SUGAR.AutoComplete.{$ac_key}.queryDelay = 0;
		SUGAR.AutoComplete.{$ac_key}.numOptions = {$field_options|@count};
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 300) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 200;
			{literal}
		}
		{/literal}
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 3000) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 500;
			{literal}
		}
		{/literal}
		
	SUGAR.AutoComplete.{$ac_key}.optionsVisible = false;
	
	{literal}
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		{/literal}
		minQueryLength: SUGAR.AutoComplete.{$ac_key}.minQLen,
		queryDelay: SUGAR.AutoComplete.{$ac_key}.queryDelay,
		zIndex: 99999,

				
		{literal}
		source: SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds,
		
		resultTextLocator: 'text',
		resultHighlighter: 'phraseMatch',
		resultFilters: 'phraseMatch',
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName('dccontent');
		if(hover[0] != null){
			if (ex) {
				var h = '1000px';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = '';
			}
		}
	}
		
	if({/literal}SUGAR.AutoComplete.{$ac_key}.minQLen{literal} == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function () {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.sendRequest('');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('click', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('click');
		});
		
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('dblclick', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('dblclick');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('focus');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mouseup', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mouseup');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mousedown', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mousedown');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('blur', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('blur');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = false;
			var selectElem = document.getElementById("{/literal}{$fields.estatus_orden.name}{literal}");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputImage.ancestor().on('click', function () {
		if (SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.blur();
		} else {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('query', function () {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.set('value', '');
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('visibleChange', function (e) {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('select', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
{/literal}
{/if}
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_NAME">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_NAME' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="name" field="name"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.name.value) <= 0}
{assign var="value" value=$fields.name.default_value }
{else}
{assign var="value" value=$fields.name.value }
{/if}  
<input type='text' name='{$fields.name.name}' 
id='{$fields.name.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_ASESOR">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_ASESOR' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="asesor"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.asesor.value) <= 0}
{assign var="value" value=$fields.asesor.default_value }
{else}
{assign var="value" value=$fields.asesor.value }
{/if}  
<input type='text' name='{$fields.asesor.name}' 
id='{$fields.asesor.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_ASIGNACION">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_ASIGNACION' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="asignacion"  >
{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.asignacion.name}" class="sqsEnabled" tabindex="0" id="{$fields.asignacion.name}" size="" value="{$fields.asignacion.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.asignacion.id_name}" 
id="{$fields.asignacion.id_name}" 
value="{$fields.awr_asignaciones_id_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.asignacion.name}" id="btn_{$fields.asignacion.name}" tabindex="0" title="{sugar_translate label="LBL_SELECT_BUTTON_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_SELECT_BUTTON_LABEL"}"
onclick='open_popup(
"{$fields.asignacion.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"awr_asignaciones_id_c","name":"asignacion"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.asignacion.name}" id="btn_clr_{$fields.asignacion.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_RELATE_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.asignacion.name}', '{$fields.asignacion.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_RELATE_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.asignacion.name}']) != 'undefined'",
		enableQS
);
</script>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_NUMERO_SINIESTRO_TXT">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_NUMERO_SINIESTRO_TXT' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="numero_siniestro_txt"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.numero_siniestro_txt.value) <= 0}
{assign var="value" value=$fields.numero_siniestro_txt.default_value }
{else}
{assign var="value" value=$fields.numero_siniestro_txt.value }
{/if}  
<input type='text' name='{$fields.numero_siniestro_txt.name}' 
id='{$fields.numero_siniestro_txt.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MARCA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MARCA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="marca"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.marca.value) <= 0}
{assign var="value" value=$fields.marca.default_value }
{else}
{assign var="value" value=$fields.marca.value }
{/if}  
<input type='text' name='{$fields.marca.name}' 
id='{$fields.marca.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MODELO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MODELO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="modelo"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.modelo.value) <= 0}
{assign var="value" value=$fields.modelo.default_value }
{else}
{assign var="value" value=$fields.modelo.value }
{/if}  
<input type='text' name='{$fields.modelo.name}' 
id='{$fields.modelo.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_ANIO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_ANIO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="anio"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.anio.value) <= 0}
{assign var="value" value=$fields.anio.default_value }
{else}
{assign var="value" value=$fields.anio.value }
{/if}  
<input type='text' name='{$fields.anio.name}' 
id='{$fields.anio.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_NUM_SERIE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_NUM_SERIE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="num_serie"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.num_serie.value) <= 0}
{assign var="value" value=$fields.num_serie.default_value }
{else}
{assign var="value" value=$fields.num_serie.value }
{/if}  
<input type='text' name='{$fields.num_serie.name}' 
id='{$fields.num_serie.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_0" data-id="LBL_EDITVIEW_PANEL1">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_COSMETICA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_COSMETICA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="cosmetica"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.cosmetica.value) == "1" || strval($fields.cosmetica.value) == "yes" || strval($fields.cosmetica.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.cosmetica.name}" value="0"> 
<input type="checkbox" id="{$fields.cosmetica.name}" 
name="{$fields.cosmetica.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_COS_ENDEREZADO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_COS_ENDEREZADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="cos_enderezado"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.cos_enderezado.value) == "1" || strval($fields.cos_enderezado.value) == "yes" || strval($fields.cos_enderezado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.cos_enderezado.name}" value="0"> 
<input type="checkbox" id="{$fields.cos_enderezado.name}" 
name="{$fields.cos_enderezado.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_REEMPLAZO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_REEMPLAZO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="reemplazo"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.reemplazo.value) == "1" || strval($fields.reemplazo.value) == "yes" || strval($fields.reemplazo.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.reemplazo.name}" value="0"> 
<input type="checkbox" id="{$fields.reemplazo.name}" 
name="{$fields.reemplazo.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_FISURA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_FISURA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="fisura"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.fisura.value) == "1" || strval($fields.fisura.value) == "yes" || strval($fields.fisura.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.fisura.name}" value="0"> 
<input type="checkbox" id="{$fields.fisura.name}" 
name="{$fields.fisura.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_ENDEREZADO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_ENDEREZADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="enderezado"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.enderezado.value) == "1" || strval($fields.enderezado.value) == "yes" || strval($fields.enderezado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.enderezado.name}" value="0"> 
<input type="checkbox" id="{$fields.enderezado.name}" 
name="{$fields.enderezado.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_PERDIDA_METAL">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_PERDIDA_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="perdida_metal"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.perdida_metal.value) == "1" || strval($fields.perdida_metal.value) == "yes" || strval($fields.perdida_metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.perdida_metal.name}" value="0"> 
<input type="checkbox" id="{$fields.perdida_metal.name}" 
name="{$fields.perdida_metal.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_REMANUFACTURA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_REMANUFACTURA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="remanufactura"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.remanufactura.value) == "1" || strval($fields.remanufactura.value) == "yes" || strval($fields.remanufactura.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.remanufactura.name}" value="0"> 
<input type="checkbox" id="{$fields.remanufactura.name}" 
name="{$fields.remanufactura.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_OTRO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_OTRO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="otro"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.otro.value) <= 0}
{assign var="value" value=$fields.otro.default_value }
{else}
{assign var="value" value=$fields.otro.value }
{/if}  
<input type='text' name='{$fields.otro.name}' 
id='{$fields.otro.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DANIO_DG">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DANIO_DG' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="danio_dg"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.danio_dg.value) == "1" || strval($fields.danio_dg.value) == "yes" || strval($fields.danio_dg.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.danio_dg.name}" value="0"> 
<input type="checkbox" id="{$fields.danio_dg.name}" 
name="{$fields.danio_dg.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DANIO_DL">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DANIO_DL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="danio_dl"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.danio_dl.value) == "1" || strval($fields.danio_dl.value) == "yes" || strval($fields.danio_dl.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.danio_dl.name}" value="0"> 
<input type="checkbox" id="{$fields.danio_dl.name}" 
name="{$fields.danio_dl.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL2' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_1" data-id="LBL_EDITVIEW_PANEL2">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DEL_DER">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DEL_DER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="del_der"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.del_der.value) == "1" || strval($fields.del_der.value) == "yes" || strval($fields.del_der.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.del_der.name}" value="0"> 
<input type="checkbox" id="{$fields.del_der.name}" 
name="{$fields.del_der.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DEL_IZQ">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DEL_IZQ' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="del_izq"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.del_izq.value) == "1" || strval($fields.del_izq.value) == "yes" || strval($fields.del_izq.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.del_izq.name}" value="0"> 
<input type="checkbox" id="{$fields.del_izq.name}" 
name="{$fields.del_izq.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_TRAS_DER">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_TRAS_DER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="tras_der"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.tras_der.value) == "1" || strval($fields.tras_der.value) == "yes" || strval($fields.tras_der.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.tras_der.name}" value="0"> 
<input type="checkbox" id="{$fields.tras_der.name}" 
name="{$fields.tras_der.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_TRAS_IZQ">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_TRAS_IZQ' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="tras_izq"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.tras_izq.value) == "1" || strval($fields.tras_izq.value) == "yes" || strval($fields.tras_izq.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.tras_izq.name}" value="0"> 
<input type="checkbox" id="{$fields.tras_izq.name}" 
name="{$fields.tras_izq.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL3' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_2" data-id="LBL_EDITVIEW_PANEL3">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_HOLLANDER">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_HOLLANDER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="hollander"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.hollander.value) <= 0}
{assign var="value" value=$fields.hollander.default_value }
{else}
{assign var="value" value=$fields.hollander.value }
{/if}  
<input type='text' name='{$fields.hollander.name}' 
id='{$fields.hollander.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_HOL_CANTIDAD">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_HOL_CANTIDAD' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="int" field="hol_cantidad"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.hol_cantidad.value) <= 0}
{assign var="value" value=$fields.hol_cantidad.default_value }
{else}
{assign var="value" value=$fields.hol_cantidad.value }
{/if}  
<input type='text' name='{$fields.hol_cantidad.name}' 
id='{$fields.hol_cantidad.name}' size='30' maxlength='255' value='{sugar_number_format precision=0 var=$value}' title='' tabindex='0'    >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_RIN_SIZE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_SIZE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="rin_size"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.rin_size.value) <= 0}
{assign var="value" value=$fields.rin_size.default_value }
{else}
{assign var="value" value=$fields.rin_size.value }
{/if}  
<input type='text' name='{$fields.rin_size.name}' 
id='{$fields.rin_size.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL4' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_3" data-id="LBL_EDITVIEW_PANEL4">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_ALUMINIO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_ALUMINIO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="aluminio"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.aluminio.value) == "1" || strval($fields.aluminio.value) == "yes" || strval($fields.aluminio.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.aluminio.name}" value="0"> 
<input type="checkbox" id="{$fields.aluminio.name}" 
name="{$fields.aluminio.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_ACERO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_ACERO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="acero"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.acero.value) == "1" || strval($fields.acero.value) == "yes" || strval($fields.acero.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.acero.name}" value="0"> 
<input type="checkbox" id="{$fields.acero.name}" 
name="{$fields.acero.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CROMADO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_CROMADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="cromado"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.cromado.value) == "1" || strval($fields.cromado.value) == "yes" || strval($fields.cromado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.cromado.name}" value="0"> 
<input type="checkbox" id="{$fields.cromado.name}" 
name="{$fields.cromado.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_PULIDO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_PULIDO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="pulido"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.pulido.value) == "1" || strval($fields.pulido.value) == "yes" || strval($fields.pulido.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.pulido.name}" value="0"> 
<input type="checkbox" id="{$fields.pulido.name}" 
name="{$fields.pulido.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DIAMANTADO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DIAMANTADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="diamantado"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.diamantado.value) == "1" || strval($fields.diamantado.value) == "yes" || strval($fields.diamantado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.diamantado.name}" value="0"> 
<input type="checkbox" id="{$fields.diamantado.name}" 
name="{$fields.diamantado.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_PINTADO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_PINTADO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="pintado"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.pintado.value) == "1" || strval($fields.pintado.value) == "yes" || strval($fields.pintado.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.pintado.name}" value="0"> 
<input type="checkbox" id="{$fields.pintado.name}" 
name="{$fields.pintado.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_HYPER">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_HYPER' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="hyper"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.hyper.value) == "1" || strval($fields.hyper.value) == "yes" || strval($fields.hyper.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.hyper.name}" value="0"> 
<input type="checkbox" id="{$fields.hyper.name}" 
name="{$fields.hyper.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CENTRO_RIN">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_CENTRO_RIN' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="centro_rin"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.centro_rin.value) == "1" || strval($fields.centro_rin.value) == "yes" || strval($fields.centro_rin.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.centro_rin.name}" value="0"> 
<input type="checkbox" id="{$fields.centro_rin.name}" 
name="{$fields.centro_rin.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL5' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_4" data-id="LBL_EDITVIEW_PANEL5">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_METAL">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="metal"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.metal.value) == "1" || strval($fields.metal.value) == "yes" || strval($fields.metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.metal.name}" value="0"> 
<input type="checkbox" id="{$fields.metal.name}" 
name="{$fields.metal.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_HULE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_HULE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="hule"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.hule.value) == "1" || strval($fields.hule.value) == "yes" || strval($fields.hule.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.hule.name}" value="0"> 
<input type="checkbox" id="{$fields.hule.name}" 
name="{$fields.hule.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CROMO">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_CROMO' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="cromo"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.cromo.value) == "1" || strval($fields.cromo.value) == "yes" || strval($fields.cromo.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.cromo.name}" value="0"> 
<input type="checkbox" id="{$fields.cromo.name}" 
name="{$fields.cromo.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_TAPON_METAL">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_TAPON_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="tapon_metal"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.tapon_metal.value) == "1" || strval($fields.tapon_metal.value) == "yes" || strval($fields.tapon_metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.tapon_metal.name}" value="0"> 
<input type="checkbox" id="{$fields.tapon_metal.name}" 
name="{$fields.tapon_metal.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_SENSOR">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_SENSOR' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="sensor"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.sensor.value) == "1" || strval($fields.sensor.value) == "yes" || strval($fields.sensor.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.sensor.name}" value="0"> 
<input type="checkbox" id="{$fields.sensor.name}" 
name="{$fields.sensor.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_TAPON_PIVOTE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_TAPON_PIVOTE' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="tapon_pivote"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.tapon_pivote.value) <= 0}
{assign var="value" value=$fields.tapon_pivote.default_value }
{else}
{assign var="value" value=$fields.tapon_pivote.value }
{/if}  
<input type='text' name='{$fields.tapon_pivote.name}' 
id='{$fields.tapon_pivote.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_COMENTARIOS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_COMENTARIOS' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="text" field="comentarios"  >
{counter name="panelFieldCount" print=false}

{if empty($fields.comentarios.value)}
{assign var="value" value=$fields.comentarios.default_value }
{else}
{assign var="value" value=$fields.comentarios.value }
{/if}
<textarea  id='{$fields.comentarios.name}' name='{$fields.comentarios.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL6' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_5" data-id="LBL_EDITVIEW_PANEL6">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_PRES_RDI_NUM">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_PRES_RDI_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="pres_rdi_num"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.pres_rdi_num.value) <= 0}
{assign var="value" value=$fields.pres_rdi_num.default_value }
{else}
{assign var="value" value=$fields.pres_rdi_num.value }
{/if}  
<input type='text' name='{$fields.pres_rdi_num.name}' 
id='{$fields.pres_rdi_num.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_PRES_RDD_NUM">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_PRES_RDD_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="pres_rdd_num"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.pres_rdd_num.value) <= 0}
{assign var="value" value=$fields.pres_rdd_num.default_value }
{else}
{assign var="value" value=$fields.pres_rdd_num.value }
{/if}  
<input type='text' name='{$fields.pres_rdd_num.name}' 
id='{$fields.pres_rdd_num.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_PRES_RTI_NUM">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_PRES_RTI_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="pres_rti_num"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.pres_rti_num.value) <= 0}
{assign var="value" value=$fields.pres_rti_num.default_value }
{else}
{assign var="value" value=$fields.pres_rti_num.value }
{/if}  
<input type='text' name='{$fields.pres_rti_num.name}' 
id='{$fields.pres_rti_num.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_PRED_RTD_NUM">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_PRED_RTD_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="pred_rtd_num"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.pred_rtd_num.value) <= 0}
{assign var="value" value=$fields.pred_rtd_num.default_value }
{else}
{assign var="value" value=$fields.pred_rtd_num.value }
{/if}  
<input type='text' name='{$fields.pred_rtd_num.name}' 
id='{$fields.pred_rtd_num.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_TOTAL_RIN_NUM">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_TOTAL_RIN_NUM' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="total_rin_num"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.total_rin_num.value) <= 0}
{assign var="value" value=$fields.total_rin_num.default_value }
{else}
{assign var="value" value=$fields.total_rin_num.value }
{/if}  
<input type='text' name='{$fields.total_rin_num.name}' 
id='{$fields.total_rin_num.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL7' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_6" data-id="LBL_EDITVIEW_PANEL7">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MONTAR_USADA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MONTAR_USADA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="montar_usada"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.montar_usada.value) == "1" || strval($fields.montar_usada.value) == "yes" || strval($fields.montar_usada.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.montar_usada.name}" value="0"> 
<input type="checkbox" id="{$fields.montar_usada.name}" 
name="{$fields.montar_usada.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MONTAR_NUEVA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MONTAR_NUEVA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="montar_nueva"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.montar_nueva.value) == "1" || strval($fields.montar_nueva.value) == "yes" || strval($fields.montar_nueva.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.montar_nueva.name}" value="0"> 
<input type="checkbox" id="{$fields.montar_nueva.name}" 
name="{$fields.montar_nueva.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_INSTR_DESCR_LLANTA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_INSTR_DESCR_LLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="instr_descr_llanta"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.instr_descr_llanta.value) <= 0}
{assign var="value" value=$fields.instr_descr_llanta.default_value }
{else}
{assign var="value" value=$fields.instr_descr_llanta.value }
{/if}  
<input type='text' name='{$fields.instr_descr_llanta.name}' 
id='{$fields.instr_descr_llanta.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_INSTR_TAMANIO_LLANTA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_INSTR_TAMANIO_LLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="varchar" field="instr_tamanio_llanta"  >
{counter name="panelFieldCount" print=false}

{if strlen($fields.instr_tamanio_llanta.value) <= 0}
{assign var="value" value=$fields.instr_tamanio_llanta.default_value }
{else}
{assign var="value" value=$fields.instr_tamanio_llanta.value }
{/if}  
<input type='text' name='{$fields.instr_tamanio_llanta.name}' 
id='{$fields.instr_tamanio_llanta.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL8' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_7" data-id="LBL_EDITVIEW_PANEL8">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_UNIDAD_RECIBIDA_PLANTA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_UNIDAD_RECIBIDA_PLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="unidad_recibida_planta"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.unidad_recibida_planta.value) == "1" || strval($fields.unidad_recibida_planta.value) == "yes" || strval($fields.unidad_recibida_planta.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.unidad_recibida_planta.name}" value="0"> 
<input type="checkbox" id="{$fields.unidad_recibida_planta.name}" 
name="{$fields.unidad_recibida_planta.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_RIN_CON_LLANTA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_CON_LLANTA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="rin_con_llanta"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_con_llanta.value) == "1" || strval($fields.rin_con_llanta.value) == "yes" || strval($fields.rin_con_llanta.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.rin_con_llanta.name}" value="0"> 
<input type="checkbox" id="{$fields.rin_con_llanta.name}" 
name="{$fields.rin_con_llanta.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_RIN_APORTE_METAL">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_APORTE_METAL' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="rin_aporte_metal"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_aporte_metal.value) == "1" || strval($fields.rin_aporte_metal.value) == "yes" || strval($fields.rin_aporte_metal.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.rin_aporte_metal.name}" value="0"> 
<input type="checkbox" id="{$fields.rin_aporte_metal.name}" 
name="{$fields.rin_aporte_metal.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_RIN_2_PIEZAS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_2_PIEZAS' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="rin_2_piezas"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_2_piezas.value) == "1" || strval($fields.rin_2_piezas.value) == "yes" || strval($fields.rin_2_piezas.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.rin_2_piezas.name}" value="0"> 
<input type="checkbox" id="{$fields.rin_2_piezas.name}" 
name="{$fields.rin_2_piezas.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_RIN_EMPAPELAR">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_EMPAPELAR' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="rin_empapelar"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_empapelar.value) == "1" || strval($fields.rin_empapelar.value) == "yes" || strval($fields.rin_empapelar.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.rin_empapelar.name}" value="0"> 
<input type="checkbox" id="{$fields.rin_empapelar.name}" 
name="{$fields.rin_empapelar.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_RIN_2_TONOS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_RIN_2_TONOS' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="rin_2_tonos"  >
{counter name="panelFieldCount" print=false}

{if strval($fields.rin_2_tonos.value) == "1" || strval($fields.rin_2_tonos.value) == "yes" || strval($fields.rin_2_tonos.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.rin_2_tonos.name}" value="0"> 
<input type="checkbox" id="{$fields.rin_2_tonos.name}" 
name="{$fields.rin_2_tonos.name}" 
value="1" title='' tabindex="0" {$checked} >
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_TIPO_RIN">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_TIPO_RIN' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="tipo_rin"  >
{counter name="panelFieldCount" print=false}

{if !isset($config.enable_autocomplete) || $config.enable_autocomplete==false}
<select name="{$fields.tipo_rin.name}" 
id="{$fields.tipo_rin.name}" 
title=''       
>
{if isset($fields.tipo_rin.value) && $fields.tipo_rin.value != ''}
{html_options options=$fields.tipo_rin.options selected=$fields.tipo_rin.value}
{else}
{html_options options=$fields.tipo_rin.options selected=$fields.tipo_rin.default}
{/if}
</select>
{else}
{assign var="field_options" value=$fields.tipo_rin.options }
{capture name="field_val"}{$fields.tipo_rin.value}{/capture}
{assign var="field_val" value=$smarty.capture.field_val}
{capture name="ac_key"}{$fields.tipo_rin.name}{/capture}
{assign var="ac_key" value=$smarty.capture.ac_key}
<select style='display:none' name="{$fields.tipo_rin.name}" 
id="{$fields.tipo_rin.name}" 
title=''          
>
{if isset($fields.tipo_rin.value) && $fields.tipo_rin.value != ''}
{html_options options=$fields.tipo_rin.options selected=$fields.tipo_rin.value}
{else}
{html_options options=$fields.tipo_rin.options selected=$fields.tipo_rin.default}
{/if}
</select>
<input
id="{$fields.tipo_rin.name}-input"
name="{$fields.tipo_rin.name}-input"
size="30"
value="{$field_val|lookup:$field_options}"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="{sugar_getimagepath file="id-ff-down.png"}" id="{$fields.tipo_rin.name}-image"></button><button type="button"
id="btn-clear-{$fields.tipo_rin.name}-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '{$fields.tipo_rin.name}-input', '{$fields.tipo_rin.name}');sync_{$fields.tipo_rin.name}()"><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
{literal}
<script>
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal} = [];
	{/literal}

			{literal}
		(function (){
			var selectElem = document.getElementById("{/literal}{$fields.tipo_rin.name}{literal}");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:''};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use('datasource', 'datasource-jsonschema',function (Y) {
				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value=='' && selectElem.options[i].innerHTML==''))
				    			ret.push({'key':selectElem.options[i].value,'text':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		{/literal}
	
	{literal}
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	{/literal}
			
	SUGAR.AutoComplete.{$ac_key}.inputNode = Y.one('#{$fields.tipo_rin.name}-input');
	SUGAR.AutoComplete.{$ac_key}.inputImage = Y.one('#{$fields.tipo_rin.name}-image');
	SUGAR.AutoComplete.{$ac_key}.inputHidden = Y.one('#{$fields.tipo_rin.name}');
	
			{literal}
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("{/literal}{$fields.tipo_rin.name}{literal}");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('change');
			}

			//global variable 
			sync_{/literal}{$fields.tipo_rin.name}{literal} = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("{/literal}{$fields.tipo_rin.name}{literal}");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value');

				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.simulate('keyup');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById('{/literal}{$fields.tipo_rin.name}-input{literal}'))
						SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("{/literal}{$fields.tipo_rin.name}{literal}", syncFromHiddenToWidget);
		{/literal}

		SUGAR.AutoComplete.{$ac_key}.minQLen = 0;
		SUGAR.AutoComplete.{$ac_key}.queryDelay = 0;
		SUGAR.AutoComplete.{$ac_key}.numOptions = {$field_options|@count};
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 300) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 200;
			{literal}
		}
		{/literal}
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 3000) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 500;
			{literal}
		}
		{/literal}
		
	SUGAR.AutoComplete.{$ac_key}.optionsVisible = false;
	
	{literal}
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		{/literal}
		minQueryLength: SUGAR.AutoComplete.{$ac_key}.minQLen,
		queryDelay: SUGAR.AutoComplete.{$ac_key}.queryDelay,
		zIndex: 99999,

				
		{literal}
		source: SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds,
		
		resultTextLocator: 'text',
		resultHighlighter: 'phraseMatch',
		resultFilters: 'phraseMatch',
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName('dccontent');
		if(hover[0] != null){
			if (ex) {
				var h = '1000px';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = '';
			}
		}
	}
		
	if({/literal}SUGAR.AutoComplete.{$ac_key}.minQLen{literal} == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function () {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.sendRequest('');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('click', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('click');
		});
		
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('dblclick', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('dblclick');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('focus');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mouseup', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mouseup');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mousedown', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mousedown');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('blur', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('blur');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = false;
			var selectElem = document.getElementById("{/literal}{$fields.tipo_rin.name}{literal}");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputImage.ancestor().on('click', function () {
		if (SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.blur();
		} else {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('query', function () {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.set('value', '');
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('visibleChange', function (e) {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('select', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
{/literal}
{/if}
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL9' module='AWR_Ordenes'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_8" data-id="LBL_EDITVIEW_PANEL9">
<div class="tab-content">
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_IMG_FIRMA">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_IMG_FIRMA' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="image" field="img_firma"  >
{counter name="panelFieldCount" print=false}

<script type="text/javascript">
    {literal}
        $( document ).ready(function() {
        $( "form#EditView" )
        .attr( "enctype", "multipart/form-data" )
        .attr( "encoding", "multipart/form-data" );
    });
{/literal}
</script>
<script type="text/javascript" src='include/SugarFields/Fields/Image/SugarFieldFile.js?v=Dr2WPisE_x0yTZbTRa2y8w'></script>
{if !empty($fields.img_firma.value) }
{assign var=showRemove value=true}
{else}
{assign var=showRemove value=false}
{/if}
{assign var=noChange value=false}
<input type="hidden" name="deleteAttachment" value="0">
<input type="hidden" name="{$fields.img_firma.name}" id="{$fields.img_firma.name}" value="{$fields.img_firma.value}">
<input type="hidden" name="{$fields.img_firma.name}_record_id" id="{$fields.img_firma.name}_record_id" value="{$fields.id.value}">
<span id="{$fields.img_firma.name}_old" style="display:{if !$showRemove}none;{/if}">
<a href="index.php?entryPoint=download&id={$fields.id.value}_{$fields.img_firma.name}&type={$module}&time={$fields.date_modified.value}" class="tabDetailViewDFLink">{$fields.img_firma.value}</a>
{if !$noChange}
<input type='button' class='button' id='remove_button' value='{$APP.LBL_REMOVE}' onclick='SUGAR.field.file.deleteAttachment("{$fields.img_firma.name}","",this);'>
{/if}
</span>
{if !$noChange}
<span id="{$fields.img_firma.name}_new" style="display:{if $showRemove}none;{/if}">
<input type="hidden" name="{$fields.img_firma.name}_escaped">
<input id="{$fields.img_firma.name}_file" name="{$fields.img_firma.name}_file"
type="file" title='' size="30"
maxlength='255'
>
{else}

{/if}
<script type="text/javascript">
$( "#{$fields.img_firma.name}_file{literal} " ).change(function() {
        $("#{/literal}{$fields.img_firma.name}{literal}").val($("#{/literal}{$fields.img_firma.name}_file{literal}").val());
});{/literal}
        </script>
</span>
</div>

<!-- [/hide] -->
</div>


<div class="col-xs-12 col-sm-6 edit-view-row-item">


<div class="col-xs-12 col-sm-4 label" data-label="LBL_IMG_ENT">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_IMG_ENT' module='AWR_Ordenes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="image" field="img_ent"  >
{counter name="panelFieldCount" print=false}

<script type="text/javascript">
    {literal}
        $( document ).ready(function() {
        $( "form#EditView" )
        .attr( "enctype", "multipart/form-data" )
        .attr( "encoding", "multipart/form-data" );
    });
{/literal}
</script>
<script type="text/javascript" src='include/SugarFields/Fields/Image/SugarFieldFile.js?v=Dr2WPisE_x0yTZbTRa2y8w'></script>
{if !empty($fields.img_ent.value) }
{assign var=showRemove value=true}
{else}
{assign var=showRemove value=false}
{/if}
{assign var=noChange value=false}
<input type="hidden" name="deleteAttachment" value="0">
<input type="hidden" name="{$fields.img_ent.name}" id="{$fields.img_ent.name}" value="{$fields.img_ent.value}">
<input type="hidden" name="{$fields.img_ent.name}_record_id" id="{$fields.img_ent.name}_record_id" value="{$fields.id.value}">
<span id="{$fields.img_ent.name}_old" style="display:{if !$showRemove}none;{/if}">
<a href="index.php?entryPoint=download&id={$fields.id.value}_{$fields.img_ent.name}&type={$module}&time={$fields.date_modified.value}" class="tabDetailViewDFLink">{$fields.img_ent.value}</a>
{if !$noChange}
<input type='button' class='button' id='remove_button' value='{$APP.LBL_REMOVE}' onclick='SUGAR.field.file.deleteAttachment("{$fields.img_ent.name}","",this);'>
{/if}
</span>
{if !$noChange}
<span id="{$fields.img_ent.name}_new" style="display:{if $showRemove}none;{/if}">
<input type="hidden" name="{$fields.img_ent.name}_escaped">
<input id="{$fields.img_ent.name}_file" name="{$fields.img_ent.name}_file"
type="file" title='' size="30"
maxlength='255'
>
{else}

{/if}
<script type="text/javascript">
$( "#{$fields.img_ent.name}_file{literal} " ).change(function() {
        $("#{/literal}{$fields.img_ent.name}{literal}").val($("#{/literal}{$fields.img_ent.name}_file{literal}").val());
});{/literal}
        </script>
</span>
</div>

<!-- [/hide] -->
</div>
<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>
</div>
</div>

<script language="javascript">
    var _form_id = '{$form_id}';
    {literal}
    SUGAR.util.doWhen(function(){
        _form_id = (_form_id == '') ? 'EditView' : _form_id;
        return document.getElementById(_form_id) != null;
    }, SUGAR.themes.actionMenu);
    {/literal}
</script>
{assign var='place' value="_FOOTER"} <!-- to be used for id for buttons with custom code in def files-->
<div class="buttons">
{if $bean->aclAccess("save")}<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('EditView'); {if $isDuplicate}_form.return_id.value=''; {/if}_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" id="SAVE">{/if} 
{if !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($smarty.request.return_id))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" type="button" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($fields.id.value))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && empty($fields.id.value)) && empty($smarty.request.return_id)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && !empty($smarty.request.return_module)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action={$smarty.request.return_action}&module={$smarty.request.return_module|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif empty($smarty.request.return_action) || empty($smarty.request.return_id) && !empty($fields.id.value)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=AWR_Ordenes'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {else}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {/if}
{if $showVCRControl}
<button type="button" id="save_and_continue" class="button saveAndContinue" title="{$app_strings.LBL_SAVE_AND_CONTINUE}" onClick="SUGAR.saveAndContinue(this);">
{$APP.LBL_SAVE_AND_CONTINUE}
</button>
{/if}
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AWR_Ordenes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>
</form>
{$set_focus_block}
<script>SUGAR.util.doWhen("document.getElementById('EditView') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script>
<script type="text/javascript">
YAHOO.util.Event.onContentReady("EditView",
    function () {ldelim} initEditView(document.forms.EditView) {rdelim});
//window.setTimeout(, 100);
window.onbeforeunload = function () {ldelim} return onUnloadEditView(); {rdelim};
// bug 55468 -- IE is too aggressive with onUnload event
if ($.browser.msie) {ldelim}
$(document).ready(function() {ldelim}
    $(".collapseLink,.expandLink").click(function (e) {ldelim} e.preventDefault(); {rdelim});
  {rdelim});
{rdelim}
</script>
{literal}
<script type="text/javascript">

    var selectTab = function(tab) {
        $('#EditView_tabs div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').hide();
        $('#EditView_tabs div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').eq(tab).show().addClass('active').addClass('in');
        $('#EditView_tabs div.panel-content div.panel').hide();
        $('#EditView_tabs div.panel-content div.panel.tab-panel-' + tab).show()
    };

    var selectTabOnError = function(tab) {
        selectTab(tab);
        $('#EditView_tabs ul.nav.nav-tabs li').removeClass('active');
        $('#EditView_tabs ul.nav.nav-tabs li a').css('color', '');

        $('#EditView_tabs ul.nav.nav-tabs li').eq(tab).find('a').first().css('color', 'red');
        $('#EditView_tabs ul.nav.nav-tabs li').eq(tab).addClass('active');

    };

    var selectTabOnErrorInputHandle = function(inputHandle) {
        var tab = $(inputHandle).closest('.tab-pane-NOBOOTSTRAPTOGGLER').attr('id').match(/^detailpanel_(.*)$/)[1];
        selectTabOnError(tab);
    };


    $(function(){
        $('#EditView_tabs ul.nav.nav-tabs li > a[data-toggle="tab"]').click(function(e){
            if(typeof $(this).parent().find('a').first().attr('id') != 'undefined') {
                var tab = parseInt($(this).parent().find('a').first().attr('id').match(/^tab(.)*$/)[1]);
                selectTab(tab);
            }
        });

        $('a[data-toggle="collapse-edit"]').click(function(e){
            if($(this).hasClass('collapsed')) {
              // Expand panel
                // Change style of .panel-header
                $(this).removeClass('collapsed');
                // Expand .panel-body
                $(this).parents('.panel').find('.panel-body').removeClass('in').addClass('in');
            } else {
              // Collapse panel
                // Change style of .panel-header
                $(this).addClass('collapsed');
                // Collapse .panel-body
                $(this).parents('.panel').find('.panel-body').removeClass('in').removeClass('in');
            }
        });
    });

    </script>
{/literal}{literal}
<script type="text/javascript">
addForm('EditView');addToValidate('EditView', 'name', 'name', true,'{/literal}{sugar_translate label='LBL_NAME' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'date_entered_date', 'date', false,'Fecha de creación' );
addToValidate('EditView', 'date_modified_date', 'date', false,'Fecha de modificación' );
addToValidate('EditView', 'modified_user_id', 'assigned_user_name', false,'{/literal}{sugar_translate label='LBL_MODIFIED' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'modified_by_name', 'relate', false,'{/literal}{sugar_translate label='LBL_MODIFIED_NAME' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'created_by', 'assigned_user_name', false,'{/literal}{sugar_translate label='LBL_CREATED' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'created_by_name', 'relate', false,'{/literal}{sugar_translate label='LBL_CREATED' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'description', 'text', false,'{/literal}{sugar_translate label='LBL_DESCRIPTION' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'deleted', 'bool', false,'{/literal}{sugar_translate label='LBL_DELETED' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'assigned_user_id', 'relate', false,'{/literal}{sugar_translate label='LBL_ASSIGNED_TO_ID' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'assigned_user_name', 'relate', false,'{/literal}{sugar_translate label='LBL_ASSIGNED_TO_NAME' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'acero', 'bool', false,'{/literal}{sugar_translate label='LBL_ACERO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'aluminio', 'bool', false,'{/literal}{sugar_translate label='LBL_ALUMINIO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'anio', 'varchar', false,'{/literal}{sugar_translate label='LBL_ANIO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'asesor', 'varchar', false,'{/literal}{sugar_translate label='LBL_ASESOR' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'centro_rin', 'bool', false,'{/literal}{sugar_translate label='LBL_CENTRO_RIN' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'comentarios', 'text', false,'{/literal}{sugar_translate label='LBL_COMENTARIOS' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'cosmetica', 'bool', false,'{/literal}{sugar_translate label='LBL_COSMETICA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'cos_enderezado', 'bool', false,'{/literal}{sugar_translate label='LBL_COS_ENDEREZADO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'cromado', 'bool', false,'{/literal}{sugar_translate label='LBL_CROMADO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'cromo', 'bool', false,'{/literal}{sugar_translate label='LBL_CROMO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'danio_dg', 'bool', false,'{/literal}{sugar_translate label='LBL_DANIO_DG' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'danio_dl', 'bool', false,'{/literal}{sugar_translate label='LBL_DANIO_DL' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'del_der', 'bool', false,'{/literal}{sugar_translate label='LBL_DEL_DER' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'del_izq', 'bool', false,'{/literal}{sugar_translate label='LBL_DEL_IZQ' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'enderezado', 'bool', false,'{/literal}{sugar_translate label='LBL_ENDEREZADO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'estatus_orden', 'enum', false,'{/literal}{sugar_translate label='LBL_ESTATUS_ORDEN' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'fisura', 'bool', false,'{/literal}{sugar_translate label='LBL_FISURA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'hollander', 'varchar', false,'{/literal}{sugar_translate label='LBL_HOLLANDER' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'hule', 'bool', false,'{/literal}{sugar_translate label='LBL_HULE' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'hyper', 'bool', false,'{/literal}{sugar_translate label='LBL_HYPER' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'img_ent', 'image', false,'{/literal}{sugar_translate label='LBL_IMG_ENT' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'img_firma', 'image', false,'{/literal}{sugar_translate label='LBL_IMG_FIRMA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'instr_descr_llanta', 'varchar', false,'{/literal}{sugar_translate label='LBL_INSTR_DESCR_LLANTA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'instr_tamanio_llanta', 'varchar', false,'{/literal}{sugar_translate label='LBL_INSTR_TAMANIO_LLANTA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'marca', 'varchar', false,'{/literal}{sugar_translate label='LBL_MARCA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'metal', 'bool', false,'{/literal}{sugar_translate label='LBL_METAL' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'modelo', 'varchar', false,'{/literal}{sugar_translate label='LBL_MODELO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'montar_nueva', 'bool', false,'{/literal}{sugar_translate label='LBL_MONTAR_NUEVA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'montar_usada', 'bool', false,'{/literal}{sugar_translate label='LBL_MONTAR_USADA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'notas', 'varchar', false,'{/literal}{sugar_translate label='LBL_NOTAS' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'numero_siniestro_txt', 'varchar', false,'{/literal}{sugar_translate label='LBL_NUMERO_SINIESTRO_TXT' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'num_serie', 'varchar', false,'{/literal}{sugar_translate label='LBL_NUM_SERIE' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'otro', 'varchar', false,'{/literal}{sugar_translate label='LBL_OTRO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'perdida_metal', 'bool', false,'{/literal}{sugar_translate label='LBL_PERDIDA_METAL' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'pintado', 'bool', false,'{/literal}{sugar_translate label='LBL_PINTADO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'pres_rdd_num', 'currency', false,'{/literal}{sugar_translate label='LBL_PRES_RDD_NUM' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'currency_id', 'currency_id', false,'{/literal}{sugar_translate label='LBL_CURRENCY' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'pres_rdi_num', 'currency', false,'{/literal}{sugar_translate label='LBL_PRES_RDI_NUM' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'pred_rtd_num', 'currency', false,'{/literal}{sugar_translate label='LBL_PRED_RTD_NUM' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'pres_rti_num', 'currency', false,'{/literal}{sugar_translate label='LBL_PRES_RTI_NUM' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'pulido', 'bool', false,'{/literal}{sugar_translate label='LBL_PULIDO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'reemplazo', 'bool', false,'{/literal}{sugar_translate label='LBL_REEMPLAZO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'remanufactura', 'bool', false,'{/literal}{sugar_translate label='LBL_REMANUFACTURA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'rin_2_piezas', 'bool', false,'{/literal}{sugar_translate label='LBL_RIN_2_PIEZAS' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'rin_2_tonos', 'bool', false,'{/literal}{sugar_translate label='LBL_RIN_2_TONOS' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'rin_aporte_metal', 'bool', false,'{/literal}{sugar_translate label='LBL_RIN_APORTE_METAL' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'rin_con_llanta', 'bool', false,'{/literal}{sugar_translate label='LBL_RIN_CON_LLANTA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'rin_empapelar', 'bool', false,'{/literal}{sugar_translate label='LBL_RIN_EMPAPELAR' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'rin_size', 'varchar', false,'{/literal}{sugar_translate label='LBL_RIN_SIZE' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'sensor', 'bool', false,'{/literal}{sugar_translate label='LBL_SENSOR' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'tapon_metal', 'bool', false,'{/literal}{sugar_translate label='LBL_TAPON_METAL' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'tapon_pivote', 'varchar', false,'{/literal}{sugar_translate label='LBL_TAPON_PIVOTE' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'tipo_rin', 'enum', false,'{/literal}{sugar_translate label='LBL_TIPO_RIN' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'total_rin_num', 'currency', false,'{/literal}{sugar_translate label='LBL_TOTAL_RIN_NUM' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'tras_der', 'bool', false,'{/literal}{sugar_translate label='LBL_TRAS_DER' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'tras_izq', 'bool', false,'{/literal}{sugar_translate label='LBL_TRAS_IZQ' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'diamantado', 'bool', false,'{/literal}{sugar_translate label='LBL_DIAMANTADO' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'hol_cantidad', 'int', false,'{/literal}{sugar_translate label='LBL_HOL_CANTIDAD' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'unidad_recibida_planta', 'bool', false,'{/literal}{sugar_translate label='LBL_UNIDAD_RECIBIDA_PLANTA' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'awr_asignaciones_id_c', 'id', false,'{/literal}{sugar_translate label='LBL_ASIGNACION_AWR_ASIGNACIONES_ID' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'asignacion', 'relate', false,'{/literal}{sugar_translate label='LBL_ASIGNACION' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidate('EditView', 'awr_ordenes_awr_asignaciones_name', 'relate', false,'{/literal}{sugar_translate label='LBL_AWR_ORDENES_AWR_ASIGNACIONES_FROM_AWR_ASIGNACIONES_TITLE' module='AWR_Ordenes' for_js=true}{literal}' );
addToValidateBinaryDependency('EditView', 'assigned_user_name', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='AWR_Ordenes' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_ASSIGNED_TO' module='AWR_Ordenes' for_js=true}{literal}', 'assigned_user_id' );
addToValidateBinaryDependency('EditView', 'asignacion', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='AWR_Ordenes' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_ASIGNACION' module='AWR_Ordenes' for_js=true}{literal}', 'awr_asignaciones_id_c' );
</script><script language="javascript">if(typeof sqs_objects == 'undefined'){var sqs_objects = new Array;}sqs_objects['EditView_asignacion']={"form":"EditView","method":"query","modules":["AWR_Asignaciones"],"group":"or","field_list":["name","id"],"populate_list":["asignacion","awr_asignaciones_id_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"Sin coincidencias"};</script>{/literal}
