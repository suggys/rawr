<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_SECURITYGROUPS' => 'Security Groups',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_ASCENDING' => 'Ascending',
  'LBL_DESCENDING' => 'Descending',
  'LBL_OPT_IN' => 'Opt In',
  'LBL_OPT_IN_PENDING_EMAIL_NOT_SENT' => 'Pending Confirm opt in, Confirm opt in not sent',
  'LBL_OPT_IN_PENDING_EMAIL_SENT' => 'Pending Confirm opt in, Confirm opt in sent',
  'LBL_OPT_IN_CONFIRMED' => 'Opted in',
  'LBL_LIST_FORM_TITLE' => 'Asignaciones List',
  'LBL_MODULE_NAME' => 'Asignaciones',
  'LBL_MODULE_TITLE' => 'Asignaciones',
  'LBL_HOMEPAGE_TITLE' => 'My Asignaciones',
  'LNK_NEW_RECORD' => 'Create Asignaciones',
  'LNK_LIST' => 'View Asignaciones',
  'LNK_IMPORT_AWR_ASIGNACIONES' => 'Importar Asignaciones',
  'LBL_SEARCH_FORM_TITLE' => 'Search Asignaciones',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_AWR_ASIGNACIONES_SUBPANEL_TITLE' => 'Asignaciones',
  'LBL_NEW_FORM_TITLE' => 'New Asignaciones',
  'LBL_TYPE' => 'type',
  'LBL_CANTIDAD_RINES' => 'Cantidad de Rines',
  'LBL_COMPANIA' => 'Compañia',
  'LBL_CONTACTO' => 'Contacto',
  'LBL_DIRECCION' => 'Dirección',
  'LBL_EMAIL' => 'Correo electrónico',
  'LBL_ESTADO' => 'Estado',
  'LBL_ESTATUS_REPORTE' => 'Estatus del Reporte',
  'LBL_ESTATUS_RECOLECCION_ENVIOS' => 'Estatus de recolección de envíos',
  'LBL_FECHA_ASIGNACION' => 'Fecha  de Asignación',
  'LBL_FECHA_ENTREGA' => 'Fecha de entrega AWR',
  'LBL_FECHA_ENTREGA_CDR' => 'Fecha de  entrega a CDR',
  'LBL_FECHA_ENVIO_DESTINO_FINAL' => 'Fecha envio a destino final',
  'LBL_FECHA_REAL_REC' => 'Fecha real de recolección',
  'LBL_FECHA_RECOLECCION' => 'Fecha programada de recolección',
  'LBL_FOLIO_RECLAMACIONES' => 'Folio reclamaciones paquetería',
  'LBL_GAR_AREA_DESCRIPCION' => 'Descripción Garantía',
  'LBL_GAR_FECHA_GARANTIA' => 'Fecha envió garantía',
  'LBL_GAR_FECHA_REENVIO' => 'Fecha reingreso',
  'LBL_GAR_NUM' => 'Número de Garantía',
  'LBL_NO_GUIA_ENVIO' => 'Número guia de envío',
  'LBL_NUM_GUIA_REC' => 'Número guía de recolección',
  'LBL_OBSERVACIONES' => 'Observaciones',
  'LBL_HORARIO' => 'Horario',
  'LBL_POSICION_RIN' => 'Posicion rin',
  'LBL_RECIBIO' => 'Recibio',
  'LBL_SINIESTRO' => 'Reporte/Siniestro',
  'LBL_VEHICULO' => 'Vehículo',
  'LBL_ESTATUS_FINAL' => 'estatus final',
  'LBL_TALLER_AWR_TALLERES_ID' => '\'AWR --Taller\' (relacionado \'\' ID)',
  'LBL_TALLER' => 'AWR --Taller',
  'LBL_CUENTAS_ACCOUNT_ID' => '\'Cuentas\' (relacionado \'Cuenta\' ID)',
  'LBL_CUENTAS' => 'Cuentas',
);