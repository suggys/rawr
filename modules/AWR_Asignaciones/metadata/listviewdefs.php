<?php
$module_name = 'AWR_Asignaciones';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'CUENTAS' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_CUENTAS',
    'id' => 'ACCOUNT_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'ESTATUS_REPORTE' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_ESTATUS_REPORTE',
    'width' => '10%',
    'default' => true,
  ),
  'TALLER' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_TALLER',
    'id' => 'AWR_TALLERES_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'VEHICULO' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_VEHICULO',
    'width' => '10%',
    'default' => true,
  ),
  'ESTATUS_FINAL' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_ESTATUS_FINAL',
    'width' => '10%',
    'default' => true,
  ),
  'FECHA_RECOLECCION' => 
  array (
    'type' => 'date',
    'label' => 'LBL_FECHA_RECOLECCION',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => false,
  ),
);
;
?>
