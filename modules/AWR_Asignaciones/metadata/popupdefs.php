<?php
$popupMeta = array (
    'moduleMain' => 'AWR_Asignaciones',
    'varName' => 'AWR_Asignaciones',
    'orderBy' => 'awr_asignaciones.name',
    'whereClauses' => array (
  'name' => 'awr_asignaciones.name',
  'cuentas' => 'awr_asignaciones.cuentas',
  'estatus_reporte' => 'awr_asignaciones.estatus_reporte',
  'taller' => 'awr_asignaciones.taller',
  'vehiculo' => 'awr_asignaciones.vehiculo',
  'estatus_final' => 'awr_asignaciones.estatus_final',
  'fecha_recoleccion' => 'awr_asignaciones.fecha_recoleccion',
  'assigned_user_id' => 'awr_asignaciones.assigned_user_id',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'cuentas',
  5 => 'estatus_reporte',
  6 => 'taller',
  7 => 'vehiculo',
  8 => 'estatus_final',
  9 => 'fecha_recoleccion',
  10 => 'assigned_user_id',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'cuentas' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_CUENTAS',
    'id' => 'ACCOUNT_ID_C',
    'link' => true,
    'width' => '10%',
    'name' => 'cuentas',
  ),
  'estatus_reporte' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_ESTATUS_REPORTE',
    'width' => '10%',
    'name' => 'estatus_reporte',
  ),
  'taller' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_TALLER',
    'id' => 'AWR_TALLERES_ID_C',
    'link' => true,
    'width' => '10%',
    'name' => 'taller',
  ),
  'vehiculo' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_VEHICULO',
    'width' => '10%',
    'name' => 'vehiculo',
  ),
  'estatus_final' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_ESTATUS_FINAL',
    'width' => '10%',
    'name' => 'estatus_final',
  ),
  'fecha_recoleccion' => 
  array (
    'type' => 'date',
    'label' => 'LBL_FECHA_RECOLECCION',
    'width' => '10%',
    'name' => 'fecha_recoleccion',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
),
);
