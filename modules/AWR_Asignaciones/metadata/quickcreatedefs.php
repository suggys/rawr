<?php
$module_name = 'AWR_Asignaciones';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'fecha_asignacion',
            'label' => 'LBL_FECHA_ASIGNACION',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'cuentas',
            'studio' => 'visible',
            'label' => 'LBL_CUENTAS',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'siniestro',
            'label' => 'LBL_SINIESTRO',
          ),
          1 => 
          array (
            'name' => 'estatus_reporte',
            'studio' => 'visible',
            'label' => 'LBL_ESTATUS_REPORTE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'taller',
            'studio' => 'visible',
            'label' => 'LBL_TALLER',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'vehiculo',
            'label' => 'LBL_VEHICULO',
          ),
          1 => 
          array (
            'name' => 'cantidad_rines',
            'label' => 'LBL_CANTIDAD_RINES',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'posicion_rin',
            'label' => 'LBL_POSICION_RIN',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'fecha_recoleccion',
            'label' => 'LBL_FECHA_RECOLECCION',
          ),
          1 => 
          array (
            'name' => 'num_guia_rec',
            'label' => 'LBL_NUM_GUIA_REC',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'fecha_real_rec',
            'label' => 'LBL_FECHA_REAL_REC',
          ),
          1 => 
          array (
            'name' => 'fecha_entrega',
            'label' => 'LBL_FECHA_ENTREGA',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'no_guia_envio',
            'label' => 'LBL_NO_GUIA_ENVIO',
          ),
          1 => 
          array (
            'name' => 'fecha_envio_destino_final',
            'label' => 'LBL_FECHA_ENVIO_DESTINO_FINAL',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'fecha_entrega_cdr',
            'label' => 'LBL_FECHA_ENTREGA_CDR',
          ),
          1 => 
          array (
            'name' => 'folio_reclamaciones',
            'label' => 'LBL_FOLIO_RECLAMACIONES',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'estatus_recoleccion_envios',
            'label' => 'LBL_ESTATUS_RECOLECCION_ENVIOS',
          ),
          1 => 
          array (
            'name' => 'recibio',
            'label' => 'LBL_RECIBIO',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'observaciones',
            'studio' => 'visible',
            'label' => 'LBL_OBSERVACIONES',
          ),
          1 => 
          array (
            'name' => 'horario',
            'label' => 'LBL_HORARIO',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'estatus_final',
            'studio' => 'visible',
            'label' => 'LBL_ESTATUS_FINAL',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'gar_num',
            'label' => 'LBL_GAR_NUM',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'gar_fecha_reenvio',
            'label' => 'LBL_GAR_FECHA_REENVIO',
          ),
          1 => 
          array (
            'name' => 'gar_fecha_garantia',
            'label' => 'LBL_GAR_FECHA_GARANTIA',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'gar_area_descripcion',
            'studio' => 'visible',
            'label' => 'LBL_GAR_AREA_DESCRIPCION',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
