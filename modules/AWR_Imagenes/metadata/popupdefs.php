<?php
$popupMeta = array (
    'moduleMain' => 'AWR_Imagenes',
    'varName' => 'AWR_Imagenes',
    'orderBy' => 'awr_imagenes.name',
    'whereClauses' => array (
  'name' => 'awr_imagenes.name',
  'imagen' => 'awr_imagenes.imagen',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'imagen',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'imagen' => 
  array (
    'type' => 'image',
    'studio' => 'visible',
    'width' => '10%',
    'label' => 'LBL_IMAGEN',
    'name' => 'imagen',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'IMAGEN' => 
  array (
    'type' => 'image',
    'studio' => 'visible',
    'width' => '10%',
    'label' => 'LBL_IMAGEN',
    'default' => true,
    'name' => 'imagen',
  ),
),
);
