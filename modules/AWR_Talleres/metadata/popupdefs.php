<?php
$popupMeta = array (
    'moduleMain' => 'AWR_Talleres',
    'varName' => 'AWR_Talleres',
    'orderBy' => 'awr_talleres.name',
    'whereClauses' => array (
  'name' => 'awr_talleres.name',
  'cliente' => 'awr_talleres.cliente',
  'domicilio' => 'awr_talleres.domicilio',
  'telefono' => 'awr_talleres.telefono',
  'email' => 'awr_talleres.email',
  'assigned_user_id' => 'awr_talleres.assigned_user_id',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'cliente',
  5 => 'domicilio',
  6 => 'telefono',
  7 => 'email',
  8 => 'assigned_user_id',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'cliente' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_CLIENTE',
    'id' => 'ACCOUNT_ID_C',
    'link' => true,
    'width' => '10%',
    'name' => 'cliente',
  ),
  'domicilio' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_DOMICILIO',
    'width' => '10%',
    'name' => 'domicilio',
  ),
  'telefono' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_TELEFONO',
    'width' => '10%',
    'name' => 'telefono',
  ),
  'email' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_EMAIL',
    'width' => '10%',
    'name' => 'email',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
),
);
