<?php
$popupMeta = array (
    'moduleMain' => 'AWR_Procesos',
    'varName' => 'AWR_Procesos',
    'orderBy' => 'awr_procesos.name',
    'whereClauses' => array (
  'name' => 'awr_procesos.name',
  'ordenes_servicio' => 'awr_procesos.ordenes_servicio',
  'status' => 'awr_procesos.status',
  'tipo' => 'awr_procesos.tipo',
  'usuario' => 'awr_procesos.usuario',
  'date_entered' => 'awr_procesos.date_entered',
),
    'searchInputs' => array (
  1 => 'name',
  3 => 'status',
  4 => 'ordenes_servicio',
  5 => 'tipo',
  6 => 'usuario',
  7 => 'date_entered',
),
    'searchdefs' => array (
  'ordenes_servicio' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_ORDENES_SERVICIO',
    'id' => 'AWR_ORDENES_ID_C',
    'link' => true,
    'width' => '10%',
    'name' => 'ordenes_servicio',
  ),
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'name' => 'status',
  ),
  'tipo' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_TIPO',
    'width' => '10%',
    'name' => 'tipo',
  ),
  'usuario' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_USUARIO',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => '10%',
    'name' => 'usuario',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'name' => 'date_entered',
  ),
),
    'listviewdefs' => array (
  'ORDENES_SERVICIO' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_ORDENES_SERVICIO',
    'id' => 'AWR_ORDENES_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'name' => 'ordenes_servicio',
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
    'name' => 'status',
  ),
  'TIPO' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_TIPO',
    'width' => '10%',
    'default' => true,
    'name' => 'tipo',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_entered',
  ),
  'USUARIO' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_USUARIO',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'name' => 'usuario',
  ),
),
);
