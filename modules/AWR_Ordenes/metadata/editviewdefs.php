<?php
$module_name = 'AWR_Ordenes';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL7' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL8' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL9' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'estatus_orden',
            'studio' => 'visible',
            'label' => 'LBL_ESTATUS_ORDEN',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'asesor',
            'label' => 'LBL_ASESOR',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'asignacion',
            'studio' => 'visible',
            'label' => 'LBL_ASIGNACION',
          ),
          1 => 
          array (
            'name' => 'numero_siniestro_txt',
            'label' => 'LBL_NUMERO_SINIESTRO_TXT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'marca',
            'label' => 'LBL_MARCA',
          ),
          1 => 
          array (
            'name' => 'modelo',
            'label' => 'LBL_MODELO',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'anio',
            'label' => 'LBL_ANIO',
          ),
          1 => 
          array (
            'name' => 'num_serie',
            'label' => 'LBL_NUM_SERIE',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'cosmetica',
            'label' => 'LBL_COSMETICA',
          ),
          1 => 
          array (
            'name' => 'cos_enderezado',
            'label' => 'LBL_COS_ENDEREZADO',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'reemplazo',
            'label' => 'LBL_REEMPLAZO',
          ),
          1 => 
          array (
            'name' => 'fisura',
            'label' => 'LBL_FISURA',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'enderezado',
            'label' => 'LBL_ENDEREZADO',
          ),
          1 => 
          array (
            'name' => 'perdida_metal',
            'label' => 'LBL_PERDIDA_METAL',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'remanufactura',
            'label' => 'LBL_REMANUFACTURA',
          ),
          1 => 
          array (
            'name' => 'otro',
            'label' => 'LBL_OTRO',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'danio_dg',
            'label' => 'LBL_DANIO_DG',
          ),
          1 => 
          array (
            'name' => 'danio_dl',
            'label' => 'LBL_DANIO_DL',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'del_der',
            'label' => 'LBL_DEL_DER',
          ),
          1 => 
          array (
            'name' => 'del_izq',
            'label' => 'LBL_DEL_IZQ',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'tras_der',
            'label' => 'LBL_TRAS_DER',
          ),
          1 => 
          array (
            'name' => 'tras_izq',
            'label' => 'LBL_TRAS_IZQ',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'hollander',
            'label' => 'LBL_HOLLANDER',
          ),
          1 => 
          array (
            'name' => 'hol_cantidad',
            'label' => 'LBL_HOL_CANTIDAD',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'rin_size',
            'label' => 'LBL_RIN_SIZE',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'aluminio',
            'label' => 'LBL_ALUMINIO',
          ),
          1 => 
          array (
            'name' => 'acero',
            'label' => 'LBL_ACERO',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'cromado',
            'label' => 'LBL_CROMADO',
          ),
          1 => 
          array (
            'name' => 'pulido',
            'label' => 'LBL_PULIDO',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'diamantado',
            'label' => 'LBL_DIAMANTADO',
          ),
          1 => 
          array (
            'name' => 'pintado',
            'label' => 'LBL_PINTADO',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'hyper',
            'label' => 'LBL_HYPER',
          ),
          1 => 
          array (
            'name' => 'centro_rin',
            'label' => 'LBL_CENTRO_RIN',
          ),
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'metal',
            'label' => 'LBL_METAL',
          ),
          1 => 
          array (
            'name' => 'hule',
            'label' => 'LBL_HULE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'cromo',
            'label' => 'LBL_CROMO',
          ),
          1 => 
          array (
            'name' => 'tapon_metal',
            'label' => 'LBL_TAPON_METAL',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'sensor',
            'label' => 'LBL_SENSOR',
          ),
          1 => 
          array (
            'name' => 'tapon_pivote',
            'label' => 'LBL_TAPON_PIVOTE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'comentarios',
            'studio' => 'visible',
            'label' => 'LBL_COMENTARIOS',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'pres_rdi_num',
            'label' => 'LBL_PRES_RDI_NUM',
          ),
          1 => 
          array (
            'name' => 'pres_rdd_num',
            'label' => 'LBL_PRES_RDD_NUM',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'pres_rti_num',
            'label' => 'LBL_PRES_RTI_NUM',
          ),
          1 => 
          array (
            'name' => 'pred_rtd_num',
            'label' => 'LBL_PRED_RTD_NUM',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'total_rin_num',
            'label' => 'LBL_TOTAL_RIN_NUM',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel7' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'montar_usada',
            'label' => 'LBL_MONTAR_USADA',
          ),
          1 => 
          array (
            'name' => 'montar_nueva',
            'label' => 'LBL_MONTAR_NUEVA',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'instr_descr_llanta',
            'label' => 'LBL_INSTR_DESCR_LLANTA',
          ),
          1 => 
          array (
            'name' => 'instr_tamanio_llanta',
            'label' => 'LBL_INSTR_TAMANIO_LLANTA',
          ),
        ),
      ),
      'lbl_editview_panel8' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'unidad_recibida_planta',
            'label' => 'LBL_UNIDAD_RECIBIDA_PLANTA',
          ),
          1 => 
          array (
            'name' => 'rin_con_llanta',
            'label' => 'LBL_RIN_CON_LLANTA',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'rin_aporte_metal',
            'label' => 'LBL_RIN_APORTE_METAL',
          ),
          1 => 
          array (
            'name' => 'rin_2_piezas',
            'label' => 'LBL_RIN_2_PIEZAS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'rin_empapelar',
            'label' => 'LBL_RIN_EMPAPELAR',
          ),
          1 => 
          array (
            'name' => 'rin_2_tonos',
            'label' => 'LBL_RIN_2_TONOS',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'tipo_rin',
            'studio' => 'visible',
            'label' => 'LBL_TIPO_RIN',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel9' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'img_firma',
            'studio' => 'visible',
            'label' => 'LBL_IMG_FIRMA',
          ),
          1 => 
          array (
            'name' => 'img_ent',
            'studio' => 'visible',
            'label' => 'LBL_IMG_ENT',
          ),
        ),
      ),
    ),
  ),
);
;
?>
