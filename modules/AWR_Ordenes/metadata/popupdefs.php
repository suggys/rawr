<?php
$popupMeta = array (
    'moduleMain' => 'AWR_Ordenes',
    'varName' => 'AWR_Ordenes',
    'orderBy' => 'awr_ordenes.name',
    'whereClauses' => array (
  'name' => 'awr_ordenes.name',
  'numero_siniestro_txt' => 'awr_ordenes.numero_siniestro_txt',
  'estatus_orden' => 'awr_ordenes.estatus_orden',
  'created_by_name' => 'awr_ordenes.created_by_name',
  'date_modified' => 'awr_ordenes.date_modified',
  'modified_by_name' => 'awr_ordenes.modified_by_name',
  'tipo_rin' => 'awr_ordenes.tipo_rin',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'numero_siniestro_txt',
  5 => 'estatus_orden',
  6 => 'created_by_name',
  7 => 'date_modified',
  8 => 'modified_by_name',
  9 => 'tipo_rin',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'numero_siniestro_txt' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NUMERO_SINIESTRO_TXT',
    'width' => '10%',
    'name' => 'numero_siniestro_txt',
  ),
  'estatus_orden' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_ESTATUS_ORDEN',
    'width' => '10%',
    'name' => 'estatus_orden',
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'name' => 'created_by_name',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'name' => 'date_modified',
  ),
  'modified_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_MODIFIED_NAME',
    'id' => 'MODIFIED_USER_ID',
    'width' => '10%',
    'name' => 'modified_by_name',
  ),
  'tipo_rin' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_TIPO_RIN',
    'width' => '10%',
    'name' => 'tipo_rin',
  ),
),
);
