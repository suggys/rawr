<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Asignado a usuario con Id',
  'LBL_ASSIGNED_TO_NAME' => 'Usuario',
  'LBL_SECURITYGROUPS' => 'Grupos de Seguridad',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Grupos de Seguridad',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Fecha de creación',
  'LBL_DATE_MODIFIED' => 'Fecha de modificación',
  'LBL_MODIFIED' => 'Modificado por',
  'LBL_MODIFIED_NAME' => 'Modificado por nombre',
  'LBL_CREATED' => 'Creado por',
  'LBL_DESCRIPTION' => 'Descripción',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nombre',
  'LBL_CREATED_USER' => 'Creado por usuario',
  'LBL_MODIFIED_USER' => 'Modificado por usuario',
  'LBL_LIST_NAME' => 'Nombre',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Quitar',
  'LBL_ASCENDING' => 'Ascendente',
  'LBL_DESCENDING' => 'Descendente',
  'LBL_OPT_IN' => 'Autorizar',
  'LBL_OPT_IN_PENDING_EMAIL_NOT_SENT' => 'Autorización pendiente. Confirmación no enviada',
  'LBL_OPT_IN_PENDING_EMAIL_SENT' => 'Autorización pendiente. Confirmación ya enviada',
  'LBL_OPT_IN_CONFIRMED' => 'Autorizado',
  'LBL_LIST_FORM_TITLE' => 'Ordenes de Servicio Lista',
  'LBL_MODULE_NAME' => 'Ordenes de Servicio',
  'LBL_MODULE_TITLE' => 'Ordenes de Servicio',
  'LBL_HOMEPAGE_TITLE' => 'Mi Ordenes de Servicio',
  'LNK_NEW_RECORD' => 'Crear Ordenes de Servicio',
  'LNK_LIST' => 'Vista Ordenes de Servicio',
  'LNK_IMPORT_AWR_ORDENES' => 'Importar Ordenes de Servicio',
  'LBL_SEARCH_FORM_TITLE' => 'Búsqueda Ordenes de Servicio',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Actividades',
  'LBL_AWR_ORDENES_SUBPANEL_TITLE' => 'Ordenes de Servicio',
  'LBL_NEW_FORM_TITLE' => 'Nuevo Ordenes de Servicio',
  'LBL_ACERO' => 'Acero',
  'LBL_ALUMINIO' => 'Aluminio',
  'LBL_ANIO' => 'Año',
  'LBL_ASESOR' => 'Asesor',
  'LBL_CENTRORIN' => 'Centro Rin',
  'LBL_CENTRO_RIN' => 'Centro Rin',
  'LBL_COMENTARIOS' => 'Comentarios',
  'LBL_COSMETICA' => 'Cosmetica',
  'LBL_COS_ENDEREZADO' => 'Cosmetica y enderezado',
  'LBL_CROMADO' => 'Cromado',
  'LBL_CROMO' => 'Cromo',
  'LBL_DANIO_DG' => 'Tipo de daño DG',
  'LBL_DANIO_DL' => 'Tipo de daño DL',
  'LBL_DEL_DER' => 'Delantero Derecho',
  'LBL_DEL_IZQ' => 'Delantero Izquierdo',
  'LBL_ENDEREZADO' => 'Enderezado',
  'LBL_ESTATUS_ORDEN' => 'Estatus de la Orden',
  'LBL_FISURA' => 'Fisura',
  'LBL_HOLLANDER' => 'Hollander',
  'LBL_HULE' => 'Hule',
  'LBL_HYPER' => 'Hyper',
  'LBL_IMG_ENT' => 'Firma Entrega',
  'LBL_IMG_FIRMA' => 'Firma',
  'LBL_INSTR_DESCR_LLANTA' => 'Descripción de la llanta',
  'LBL_INSTR_TAMANIO_LLANTA' => 'Tamaño de la llanta',
  'LBL_MARCA' => 'Marca',
  'LBL_METAL' => 'Metal',
  'LBL_MODELO' => 'Modelo',
  'LBL_MONTAR_NUEVA' => 'Montar llanta nueva',
  'LBL_MONTAR_USADA' => 'Montar llanta usada',
  'LBL_NOTAS' => 'Notas',
  'LBL_NUMERO_SINIESTRO_TXT' => 'No. Siniestro',
  'LBL_NUM_SERIE' => 'Número de Serie',
  'LBL_NUM_SINIESTRO' => 'Número de siniestro',
  'LBL_OTRO' => 'Otro',
  'LBL_PERDIDA_METAL' => 'Perdida metal',
  'LBL_PINTADO' => 'Pintado',
  'LBL_PRES_RDD_NUM' => 'RDD',
  'LBL_PRES_RDI_NUM' => 'RDI',
  'LBL_PRED_RTD_NUM' => 'RTD',
  'LBL_PRES_RTI_NUM' => 'RTI',
  'LBL_PULIDO' => 'Pulido',
  'LBL_REEMPLAZO' => 'Reemplazo',
  'LBL_REMANUFACTURA' => 'Remanufactura',
  'LBL_RIN_2_PIEZAS' => 'Rin es de 2 piezas',
  'LBL_RIN_2_TONOS' => 'Rin es de 2 tonos',
  'LBL_RIN_APORTE_METAL' => 'Rin requiere aporte de metal',
  'LBL_RIN_CON_LLANTA' => 'Rin con llanta',
  'LBL_RIN_EMPAPELAR' => 'Rin requiere empapelar',
  'LBL_RIN_SIZE' => 'Tamaño de Rin',
  'LBL_SENSOR' => 'Sensor',
  'LBL_TAPON_METAL' => 'Tapon de metal',
  'LBL_TAPON_PIVOTE' => 'Tapon de pivote',
  'LBL_TIPO_RIN' => 'Tipo de reparación',
  'LBL_TOTAL_RIN_NUM' => 'Total',
  'LBL_TRAS_DER' => 'Trasero Derecho',
  'LBL_TRAS_IZQ' => 'Trasero Izquierdo',
  'LBL_EDITVIEW_PANEL1' => 'DESCRIPCIÓN DE LA REPARACIÓN',
  'LBL_EDITVIEW_PANEL2' => 'POSICIÓN DEL RIN',
  'LBL_EDITVIEW_PANEL3' => 'INSTRUCCIONES DE REPARACIÓN DEL RIN',
  'LBL_EDITVIEW_PANEL4' => 'ACABADO',
  'LBL_EDITVIEW_PANEL5' => 'PIVOTE',
  'LBL_EDITVIEW_PANEL6' => 'PRESUPUESTO',
  'LBL_EDITVIEW_PANEL7' => 'INSTRUCCIONES',
  'LBL_EDITVIEW_PANEL8' => 'TIPO DE REPARACIÓN',
  'LBL_EDITVIEW_PANEL9' => 'ACEPTACIÓN DEL USUARIO',
  'LBL_DIAMANTADO' => 'Diamantado',
  'LBL_HOL_CANTIDAD' => 'Cantidad',
  'LBL_UNIDAD_RECIBIDA_PLANTA' => 'Unidad del cliente recibida en planta',
  'LBL_QUICKCREATE_PANEL1' => 'DESCRIPCIÓN DE LA REPARACIÓN',
  'LBL_QUICKCREATE_PANEL2' => 'POSICIÓN DEL RIN',
  'LBL_QUICKCREATE_PANEL3' => 'INSTRUCCIONES DE REPARACIÓN DEL RIN',
  'LBL_QUICKCREATE_PANEL4' => 'ACABADO',
  'LBL_QUICKCREATE_PANEL5' => 'PIVOTE',
  'LBL_QUICKCREATE_PANEL6' => 'PRESUPUESTO',
  'LBL_QUICKCREATE_PANEL7' => 'INSTRUCCIONES',
  'LBL_QUICKCREATE_PANEL8' => 'TIPO DE REPARACIÓN',
  'LBL_QUICKCREATE_PANEL9' => 'ACEPTACIÓN USUARIO',
  'LBL_ASIGNACION_AWR_ASIGNACIONES_ID' => '\'Asignación\' (relacionado \'\' ID)',
  'LBL_ASIGNACION' => 'Asignación',
);