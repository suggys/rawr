<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_SECURITYGROUPS' => 'Security Groups',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_ASCENDING' => 'Ascending',
  'LBL_DESCENDING' => 'Descending',
  'LBL_OPT_IN' => 'Opt In',
  'LBL_OPT_IN_PENDING_EMAIL_NOT_SENT' => 'Pending Confirm opt in, Confirm opt in not sent',
  'LBL_OPT_IN_PENDING_EMAIL_SENT' => 'Pending Confirm opt in, Confirm opt in sent',
  'LBL_OPT_IN_CONFIRMED' => 'Opted in',
  'LBL_LIST_FORM_TITLE' => 'Ordenes de Servicio List',
  'LBL_MODULE_NAME' => 'Ordenes de Servicio',
  'LBL_MODULE_TITLE' => 'Ordenes de Servicio',
  'LBL_HOMEPAGE_TITLE' => 'My Ordenes de Servicio',
  'LNK_NEW_RECORD' => 'Create Ordenes de Servicio',
  'LNK_LIST' => 'View Ordenes de Servicio',
  'LNK_IMPORT_AWR_ORDENES' => 'Importar Ordenes de Servicio',
  'LBL_SEARCH_FORM_TITLE' => 'Search Ordenes de Servicio',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_AWR_ORDENES_SUBPANEL_TITLE' => 'Ordenes de Servicio',
  'LBL_NEW_FORM_TITLE' => 'New Ordenes de Servicio',
  'LBL_ACERO' => 'Acero',
  'LBL_ALUMINIO' => 'Aluminio',
  'LBL_ANIO' => 'año',
  'LBL_ASESOR' => 'Asesor',
  'LBL_CENTRORIN' => 'Centro Rin',
  'LBL_CENTRO_RIN' => 'Centro Rin',
  'LBL_COMENTARIOS' => 'Comentarios',
  'LBL_COSMETICA' => 'Cosmetica',
  'LBL_COS_ENDEREZADO' => 'cos enderezado',
  'LBL_CROMADO' => 'Cromado',
  'LBL_CROMO' => 'Cromo',
  'LBL_DANIO_DG' => 'Tipo de daño DG',
  'LBL_DANIO_DL' => 'Tipo de daño DL',
  'LBL_DEL_DER' => 'Delantero Derecho',
  'LBL_DEL_IZQ' => 'Delantero Izquierdo',
  'LBL_ENDEREZADO' => 'Enderezado',
  'LBL_ESTATUS_ORDEN' => 'Estatus de la Orden',
  'LBL_FISURA' => 'Fisura',
  'LBL_HOLLANDER' => 'Hollander',
  'LBL_HULE' => 'Hule',
  'LBL_HYPER' => 'Hyper',
  'LBL_IMG_ENT' => 'Firma Entrega',
  'LBL_IMG_FIRMA' => 'img firma',
  'LBL_INSTR_DESCR_LLANTA' => 'Descripción de la llanta',
  'LBL_INSTR_TAMANIO_LLANTA' => 'Tamaño de la llanta',
  'LBL_MARCA' => 'Marca',
  'LBL_METAL' => 'Metal',
  'LBL_MODELO' => 'Modelo',
  'LBL_MONTAR_NUEVA' => 'Montar llanta nueva',
  'LBL_MONTAR_USADA' => 'Montar llanta usada',
  'LBL_NOTAS' => 'Notas',
  'LBL_NUMERO_SINIESTRO_TXT' => 'No. Siniestro',
  'LBL_NUM_SERIE' => 'Número de Serie',
  'LBL_NUM_SINIESTRO' => 'Número de siniestro',
  'LBL_OTRO' => 'Otro',
  'LBL_PERDIDA_METAL' => 'Perdida metal',
  'LBL_PINTADO' => 'Pintado',
  'LBL_PRES_RDD_NUM' => 'RDD',
  'LBL_PRES_RDI_NUM' => 'RDI',
  'LBL_PRED_RTD_NUM' => 'RTD',
  'LBL_PRES_RTI_NUM' => 'RTI',
  'LBL_PULIDO' => 'pulido',
  'LBL_REEMPLAZO' => 'Reemplazo',
  'LBL_REMANUFACTURA' => 'Remanufactura',
  'LBL_RIN_2_PIEZAS' => 'Rin es de 2 piezas',
  'LBL_RIN_2_TONOS' => 'Rin es de 2 tonos',
  'LBL_RIN_APORTE_METAL' => 'Rin aporte metal',
  'LBL_RIN_CON_LLANTA' => 'Rin con llanta',
  'LBL_RIN_EMPAPELAR' => 'Rin requiere empapelar',
  'LBL_RIN_SIZE' => 'Tamaño de Rin',
  'LBL_SENSOR' => 'Sensor',
  'LBL_TAPON_METAL' => 'Tapon de metal',
  'LBL_TAPON_PIVOTE' => 'Tapon de pivote',
  'LBL_TIPO_RIN' => 'tipo rin',
  'LBL_TOTAL_RIN_NUM' => 'Total',
  'LBL_TRAS_DER' => 'Trasero Derecho',
  'LBL_TRAS_IZQ' => 'Trasero Izquierdo',
  'LBL_DIAMANTADO' => 'Diamantado',
  'LBL_HOL_CANTIDAD' => 'Cantidad',
  'LBL_UNIDAD_RECIBIDA_PLANTA' => 'Unidad del cliente recibida en planta',
  'LBL_ASIGNACION_AWR_ASIGNACIONES_ID' => '\'Asignación\' (relacionado \'\' ID)',
  'LBL_ASIGNACION' => 'Asignación',
);