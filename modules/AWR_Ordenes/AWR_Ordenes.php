<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */


class AWR_Ordenes extends Basic
{
    public $new_schema = true;
    public $module_dir = 'AWR_Ordenes';
    public $object_name = 'AWR_Ordenes';
    public $table_name = 'awr_ordenes';
    public $importable = false;

    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $SecurityGroups;
    public $acero;
    public $aluminio;
    public $anio;
    public $asesor;
    public $centro_rin;
    public $comentarios;
    public $cosmetica;
    public $cos_enderezado;
    public $cromado;
    public $cromo;
    public $danio_dg;
    public $danio_dl;
    public $del_der;
    public $del_izq;
    public $enderezado;
    public $estatus_orden;
    public $fisura;
    public $hollander;
    public $hule;
    public $hyper;
    public $img_ent;
    public $img_firma;
    public $instr_descr_llanta;
    public $instr_tamanio_llanta;
    public $marca;
    public $metal;
    public $modelo;
    public $montar_nueva;
    public $montar_usada;
    public $notas;
    public $numero_siniestro_txt;
    public $num_serie;
    public $num_siniestro;
    public $otro;
    public $perdida_metal;
    public $pintado;
    public $pres_rdd_num;
    public $currency_id;
    public $pres_rdi_num;
    public $pred_rtd_num;
    public $pres_rti_num;
    public $pulido;
    public $reemplazo;
    public $remanufactura;
    public $rin_2_piezas;
    public $rin_2_tonos;
    public $rin_aporte_metal;
    public $rin_con_llanta;
    public $rin_empapelar;
    public $rin_size;
    public $sensor;
    public $tapon_metal;
    public $tapon_pivote;
    public $tipo_rin;
    public $total_rin_num;
    public $tras_der;
    public $tras_izq;
	
    public function bean_implements($interface)
    {
        switch($interface)
        {
            case 'ACL':
                return true;
        }

        return false;
    }
	
}