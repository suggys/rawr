<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
global $current_user, $sugar_config, $db;
$id = $_REQUEST['recordId'];
$GLOBALS['log']->fatal('Formulario');
$result = $db->query("select name,asesor,numero_siniestro_txt,comentarios,marca,modelo,anio,estatus_orden from awr_ordenes orden where deleted = 0 AND orden.id = '" . $id . "'", false, 'Failed to load user preferences');
 $row = $db->fetchByAssoc($result);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Captura Firma</title>
  <meta name="description" content="Signature Pad - HTML5 canvas based smooth signature drawing using variable width spline interpolation.">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <link rel="stylesheet" href="include/signature_pad/docs/css/bootstrap.css">
  <link rel="stylesheet" href="include/signature_pad/docs/css/signature-pad.css">

</head>
<body onselectstart="return false">

  <h1 align="center">Confirmación de pedido</h1>
  <div class="container pt-5">
    <div class="row pb-3">
      <div class="span9 offset2">
        <table>
        <tbody>
        <tr>
          <td>
          <p><img style="float: left;" src="http://127.0.0.1/suitecrm/custom/themes/default/images/company_logo2.png" alt="" /> </p>
          </td>
          <td > </td>
        </tr>
        </tbody>
        </table>
        <div style="font-weight: bold; width: 100%; text-align: left;"> </div>
        <table style="font-family: Verdana, Arial, Helvetica, sans-serif; letter-spacing: 2px; text-indent: 0px; text-transform: none; word-spacing: 0px; width: 1101.11px;">
        <tbody>
        <tr>
        <td style="width: 100px;"><span><strong><span style="font-family: verdana, geneva;">Nombre:</span></strong></span></td>
        <td style="width: 150px;"><span style="font-family: verdana, geneva;"> <?php echo $row['name'] ?> </span></td>
        <td style="width: 150px;"><span><strong><span style="font-family: verdana, geneva;">Asesor:</span></strong></span></td>
        <td style="width: 150px;"><span style="font-family: verdana, geneva;"> <?php echo $row['asesor'] ?> </span></td>
        </tr>
        <tr>
        <td style="width: 100px;"><span><strong><span style="font-family: verdana, geneva;"><strong><span style="font-family: verdana, geneva;"></span></strong></span></strong></span></td>
        <td style="width: 150px;"><span style="font-family: verdana, geneva;">  </span></td>
        <td style="width: 150px;"><span><strong><span style="font-family: verdana, geneva;">No. Siniestro:</span></strong></span></td>
        <td style="width: 150px;"><span style="font-family: verdana, geneva;"> <?php echo $row['numero_siniestro_txt'] ?></span></td>
        </tr>
        <tr>
        <td style="width: 100px;"><span><strong><span style="font-family: verdana, geneva;">Marca:</span></strong></span></td>
        <td style="width: 150px;"><span style="font-family: verdana, geneva;"> <?php echo $row['marca'] ?></span></td>
        <td style="width: 150px;"><span><strong><span style="font-family: verdana, geneva;">Modelo:</span></strong></span></td>
        <td style="width: 150px;"><span style="font-family: verdana, geneva;"><?php echo $row['modelo'] ?></span></td>
        </tr>
        <tr>
        <td style="width: 100px;"><span><strong><span style="font-family: verdana, geneva;">Año:</span></strong></span></td>
        <td style="width: 150px;"><span style="font-family: verdana, geneva;"> <?php echo $row['anio'] ?></span></td>
        <td style="width: 150px;"><span><strong><span style="font-family: verdana, geneva;"></span></strong></span></td>
        <td style="width: 150px;"><span style="font-family: verdana, geneva;"> </span></td>
        </tr>             
        <tr>
        <td style="width: 100px;"><span><strong><span style="font-family: verdana, geneva;">Comentarios:</span></strong></span></td>
        <td style="width: 150px;"><span style="font-family: verdana, geneva;"> <?php echo $row['comentarios'] ?></span></td>
        <td style="width: 100px;"><span><strong><span style="font-family: verdana, geneva;"></span></strong></span></td>
        <td style="width: 150px;"><span style="font-family: verdana, geneva;"><input type="hidden" name="status_c" id="status_c" value="<?php echo $row['estatus_orden'];?>"></span></td>
        </tr>        
        <tr>
        <td style="width: 100px;"> </td>
        <td style="width: 150px;"> </td>
        <td style="width: 150px;"> </td>
        <td style="width: 150px;"> </td>
        </tr>
        </tbody>
        </table>

      </div>
    </div>
    <div class="row">
      <div class="span12 pb-5">
        <center>
        <div id="signature-pad" class="signature-pad">
          <div class="signature-pad--body">
            <canvas></canvas>
          </div>
          <div class="signature-pad--footer">
            <div class="description">Firme Aqui</div>

            <div class="signature-pad--actions">
              <div>
                <button type="button" class="btn" data-action="clear">CLEAR</button>
              </div>
            </div>
          </div>
        </div>
      </center>
      </div>
      <center class="pt-5">
        <button type="button" class="btn btn-pink px-4 save" onclick="saveJPG('<?php echo $id; ?>')">Guardar Firma</button>
      </center>
    </div>
  </div>
  
  <script src="include/signature_pad/docs/js/signature_pad.umd.js"></script>
  <script src="include/signature_pad/docs/js/app.js"></script>
  <script src="include/signature_pad/docs/js/jquery-3.3.1.min.js"></script>
</body>
</html>