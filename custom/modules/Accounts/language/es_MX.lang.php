<?php
// created: 2019-03-08 22:07:23
$mod_strings = array (
  'LNK_NEW_ACCOUNT' => 'Crear cuenta',
  'LNK_ACCOUNT_LIST' => 'Clientes',
  'LNK_IMPORT_ACCOUNTS' => 'Importar cuentas',
  'MSG_SHOW_DUPLICATES' => 'El registro para la cliente que va a crear podría ser un duplicado de otro registro de cliente existente. Los registros de cliente con nombres similares se listan a continuación.Haga clic en guardar para continuar con la creación de esta cliente, o en cancelar para volver al módulo sin crear la cliente',
  'LBL_SAVE_ACCOUNT' => 'Guardar cuenta',
  'LBL_LIST_FORM_TITLE' => 'Lista de cuentas',
  'LBL_SEARCH_FORM_TITLE' => 'Búsqueda de cuentas',
  'LBL_MEMBER_OF' => 'Miembro de:',
  'LBL_MEMBERS' => 'Miembros',
  'LBL_MEMBER_ORG_SUBPANEL_TITLE' => 'Organizaciones miembro',
  'LBL_HOMEPAGE_TITLE' => 'Mis cuentas',
  'LBL_MODULE_NAME' => 'Clientes',
);