<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2019-03-10 05:21:40
$dictionary["AWR_Ordenes"]["fields"]["awr_ordenes_awr_asignaciones"] = array (
  'name' => 'awr_ordenes_awr_asignaciones',
  'type' => 'link',
  'relationship' => 'awr_ordenes_awr_asignaciones',
  'source' => 'non-db',
  'module' => 'AWR_Asignaciones',
  'bean_name' => 'AWR_Asignaciones',
  'vname' => 'LBL_AWR_ORDENES_AWR_ASIGNACIONES_FROM_AWR_ASIGNACIONES_TITLE',
  'id_name' => 'awr_ordenes_awr_asignacionesawr_asignaciones_idb',
);
$dictionary["AWR_Ordenes"]["fields"]["awr_ordenes_awr_asignaciones_name"] = array (
  'name' => 'awr_ordenes_awr_asignaciones_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AWR_ORDENES_AWR_ASIGNACIONES_FROM_AWR_ASIGNACIONES_TITLE',
  'save' => true,
  'id_name' => 'awr_ordenes_awr_asignacionesawr_asignaciones_idb',
  'link' => 'awr_ordenes_awr_asignaciones',
  'table' => 'awr_asignaciones',
  'module' => 'AWR_Asignaciones',
  'rname' => 'name',
);
$dictionary["AWR_Ordenes"]["fields"]["awr_ordenes_awr_asignacionesawr_asignaciones_idb"] = array (
  'name' => 'awr_ordenes_awr_asignacionesawr_asignaciones_idb',
  'type' => 'link',
  'relationship' => 'awr_ordenes_awr_asignaciones',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_AWR_ORDENES_AWR_ASIGNACIONES_FROM_AWR_ASIGNACIONES_TITLE',
);


// created: 2019-03-10 05:21:39
$dictionary["AWR_Ordenes"]["fields"]["awr_ordenes_awr_procesos"] = array (
  'name' => 'awr_ordenes_awr_procesos',
  'type' => 'link',
  'relationship' => 'awr_ordenes_awr_procesos',
  'source' => 'non-db',
  'module' => 'AWR_Procesos',
  'bean_name' => 'AWR_Procesos',
  'side' => 'right',
  'vname' => 'LBL_AWR_ORDENES_AWR_PROCESOS_FROM_AWR_PROCESOS_TITLE',
);

?>