<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-03-10 05:21:39
$layout_defs["AWR_Ordenes"]["subpanel_setup"]['awr_ordenes_awr_procesos'] = array (
  'order' => 100,
  'module' => 'AWR_Procesos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AWR_ORDENES_AWR_PROCESOS_FROM_AWR_PROCESOS_TITLE',
  'get_subpanel_data' => 'awr_ordenes_awr_procesos',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>