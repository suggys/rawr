<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2019-03-10 05:21:39
$dictionary["AWR_Asignaciones"]["fields"]["awr_asignaciones_awr_talleres"] = array (
  'name' => 'awr_asignaciones_awr_talleres',
  'type' => 'link',
  'relationship' => 'awr_asignaciones_awr_talleres',
  'source' => 'non-db',
  'module' => 'AWR_Talleres',
  'bean_name' => 'AWR_Talleres',
  'vname' => 'LBL_AWR_ASIGNACIONES_AWR_TALLERES_FROM_AWR_TALLERES_TITLE',
  'id_name' => 'awr_asignaciones_awr_talleresawr_talleres_idb',
);
$dictionary["AWR_Asignaciones"]["fields"]["awr_asignaciones_awr_talleres_name"] = array (
  'name' => 'awr_asignaciones_awr_talleres_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AWR_ASIGNACIONES_AWR_TALLERES_FROM_AWR_TALLERES_TITLE',
  'save' => true,
  'id_name' => 'awr_asignaciones_awr_talleresawr_talleres_idb',
  'link' => 'awr_asignaciones_awr_talleres',
  'table' => 'awr_talleres',
  'module' => 'AWR_Talleres',
  'rname' => 'name',
);
$dictionary["AWR_Asignaciones"]["fields"]["awr_asignaciones_awr_talleresawr_talleres_idb"] = array (
  'name' => 'awr_asignaciones_awr_talleresawr_talleres_idb',
  'type' => 'link',
  'relationship' => 'awr_asignaciones_awr_talleres',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_AWR_ASIGNACIONES_AWR_TALLERES_FROM_AWR_TALLERES_TITLE',
);


// created: 2019-03-10 05:21:38
$dictionary["AWR_Asignaciones"]["fields"]["awr_asignaciones_calls"] = array (
  'name' => 'awr_asignaciones_calls',
  'type' => 'link',
  'relationship' => 'awr_asignaciones_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'side' => 'right',
  'vname' => 'LBL_AWR_ASIGNACIONES_CALLS_FROM_CALLS_TITLE',
);


// created: 2019-03-10 05:21:40
$dictionary["AWR_Asignaciones"]["fields"]["awr_ordenes_awr_asignaciones"] = array (
  'name' => 'awr_ordenes_awr_asignaciones',
  'type' => 'link',
  'relationship' => 'awr_ordenes_awr_asignaciones',
  'source' => 'non-db',
  'module' => 'AWR_Ordenes',
  'bean_name' => 'AWR_Ordenes',
  'vname' => 'LBL_AWR_ORDENES_AWR_ASIGNACIONES_FROM_AWR_ORDENES_TITLE',
  'id_name' => 'awr_ordenes_awr_asignacionesawr_ordenes_ida',
);
$dictionary["AWR_Asignaciones"]["fields"]["awr_ordenes_awr_asignaciones_name"] = array (
  'name' => 'awr_ordenes_awr_asignaciones_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AWR_ORDENES_AWR_ASIGNACIONES_FROM_AWR_ORDENES_TITLE',
  'save' => true,
  'id_name' => 'awr_ordenes_awr_asignacionesawr_ordenes_ida',
  'link' => 'awr_ordenes_awr_asignaciones',
  'table' => 'awr_ordenes',
  'module' => 'AWR_Ordenes',
  'rname' => 'name',
);
$dictionary["AWR_Asignaciones"]["fields"]["awr_ordenes_awr_asignacionesawr_ordenes_ida"] = array (
  'name' => 'awr_ordenes_awr_asignacionesawr_ordenes_ida',
  'type' => 'link',
  'relationship' => 'awr_ordenes_awr_asignaciones',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_AWR_ORDENES_AWR_ASIGNACIONES_FROM_AWR_ORDENES_TITLE',
);

?>