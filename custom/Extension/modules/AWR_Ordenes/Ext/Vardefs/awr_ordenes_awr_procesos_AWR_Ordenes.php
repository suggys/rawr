<?php
// created: 2019-03-10 05:21:39
$dictionary["AWR_Ordenes"]["fields"]["awr_ordenes_awr_procesos"] = array (
  'name' => 'awr_ordenes_awr_procesos',
  'type' => 'link',
  'relationship' => 'awr_ordenes_awr_procesos',
  'source' => 'non-db',
  'module' => 'AWR_Procesos',
  'bean_name' => 'AWR_Procesos',
  'side' => 'right',
  'vname' => 'LBL_AWR_ORDENES_AWR_PROCESOS_FROM_AWR_PROCESOS_TITLE',
);
