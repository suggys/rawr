<?php

   $hook_version = 1;
   $hook_array = Array();
   
   $hook_array['after_save'] = Array();
   $hook_array['after_save'][] = Array(
      //Processing index. For sorting the array.
      1, 

      //Label. A string value to identify the hook.
      'Guardar Firma', 

      //The PHP file where your class is located.
      'custom/modules/AWR_Ordenes/save_signature.php', 

      //The class the method is in.
      'save_signature', 

      //The method to call.
      'update_with_signature' 
   );

?>