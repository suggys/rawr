<?php
// created: 2019-03-10 05:21:38
$dictionary["Call"]["fields"]["awr_asignaciones_calls"] = array (
  'name' => 'awr_asignaciones_calls',
  'type' => 'link',
  'relationship' => 'awr_asignaciones_calls',
  'source' => 'non-db',
  'module' => 'AWR_Asignaciones',
  'bean_name' => 'AWR_Asignaciones',
  'vname' => 'LBL_AWR_ASIGNACIONES_CALLS_FROM_AWR_ASIGNACIONES_TITLE',
  'id_name' => 'awr_asignaciones_callsawr_asignaciones_ida',
);
$dictionary["Call"]["fields"]["awr_asignaciones_calls_name"] = array (
  'name' => 'awr_asignaciones_calls_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AWR_ASIGNACIONES_CALLS_FROM_AWR_ASIGNACIONES_TITLE',
  'save' => true,
  'id_name' => 'awr_asignaciones_callsawr_asignaciones_ida',
  'link' => 'awr_asignaciones_calls',
  'table' => 'awr_asignaciones',
  'module' => 'AWR_Asignaciones',
  'rname' => 'name',
);
$dictionary["Call"]["fields"]["awr_asignaciones_callsawr_asignaciones_ida"] = array (
  'name' => 'awr_asignaciones_callsawr_asignaciones_ida',
  'type' => 'link',
  'relationship' => 'awr_asignaciones_calls',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AWR_ASIGNACIONES_CALLS_FROM_CALLS_TITLE',
);
