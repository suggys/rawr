<?php
// created: 2019-03-10 05:21:39
$dictionary["AWR_Procesos"]["fields"]["awr_ordenes_awr_procesos"] = array (
  'name' => 'awr_ordenes_awr_procesos',
  'type' => 'link',
  'relationship' => 'awr_ordenes_awr_procesos',
  'source' => 'non-db',
  'module' => 'AWR_Ordenes',
  'bean_name' => 'AWR_Ordenes',
  'vname' => 'LBL_AWR_ORDENES_AWR_PROCESOS_FROM_AWR_ORDENES_TITLE',
  'id_name' => 'awr_ordenes_awr_procesosawr_ordenes_ida',
);
$dictionary["AWR_Procesos"]["fields"]["awr_ordenes_awr_procesos_name"] = array (
  'name' => 'awr_ordenes_awr_procesos_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AWR_ORDENES_AWR_PROCESOS_FROM_AWR_ORDENES_TITLE',
  'save' => true,
  'id_name' => 'awr_ordenes_awr_procesosawr_ordenes_ida',
  'link' => 'awr_ordenes_awr_procesos',
  'table' => 'awr_ordenes',
  'module' => 'AWR_Ordenes',
  'rname' => 'name',
);
$dictionary["AWR_Procesos"]["fields"]["awr_ordenes_awr_procesosawr_ordenes_ida"] = array (
  'name' => 'awr_ordenes_awr_procesosawr_ordenes_ida',
  'type' => 'link',
  'relationship' => 'awr_ordenes_awr_procesos',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AWR_ORDENES_AWR_PROCESOS_FROM_AWR_PROCESOS_TITLE',
);
