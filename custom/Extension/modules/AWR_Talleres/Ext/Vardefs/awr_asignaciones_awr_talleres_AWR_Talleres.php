<?php
// created: 2019-03-10 05:21:39
$dictionary["AWR_Talleres"]["fields"]["awr_asignaciones_awr_talleres"] = array (
  'name' => 'awr_asignaciones_awr_talleres',
  'type' => 'link',
  'relationship' => 'awr_asignaciones_awr_talleres',
  'source' => 'non-db',
  'module' => 'AWR_Asignaciones',
  'bean_name' => 'AWR_Asignaciones',
  'vname' => 'LBL_AWR_ASIGNACIONES_AWR_TALLERES_FROM_AWR_ASIGNACIONES_TITLE',
  'id_name' => 'awr_asignaciones_awr_talleresawr_asignaciones_ida',
);
$dictionary["AWR_Talleres"]["fields"]["awr_asignaciones_awr_talleres_name"] = array (
  'name' => 'awr_asignaciones_awr_talleres_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AWR_ASIGNACIONES_AWR_TALLERES_FROM_AWR_ASIGNACIONES_TITLE',
  'save' => true,
  'id_name' => 'awr_asignaciones_awr_talleresawr_asignaciones_ida',
  'link' => 'awr_asignaciones_awr_talleres',
  'table' => 'awr_asignaciones',
  'module' => 'AWR_Asignaciones',
  'rname' => 'name',
);
$dictionary["AWR_Talleres"]["fields"]["awr_asignaciones_awr_talleresawr_asignaciones_ida"] = array (
  'name' => 'awr_asignaciones_awr_talleresawr_asignaciones_ida',
  'type' => 'link',
  'relationship' => 'awr_asignaciones_awr_talleres',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_AWR_ASIGNACIONES_AWR_TALLERES_FROM_AWR_ASIGNACIONES_TITLE',
);
