<?php
// created: 2019-03-10 05:21:39
$dictionary["AWR_Asignaciones"]["fields"]["awr_asignaciones_awr_talleres"] = array (
  'name' => 'awr_asignaciones_awr_talleres',
  'type' => 'link',
  'relationship' => 'awr_asignaciones_awr_talleres',
  'source' => 'non-db',
  'module' => 'AWR_Talleres',
  'bean_name' => 'AWR_Talleres',
  'vname' => 'LBL_AWR_ASIGNACIONES_AWR_TALLERES_FROM_AWR_TALLERES_TITLE',
  'id_name' => 'awr_asignaciones_awr_talleresawr_talleres_idb',
);
$dictionary["AWR_Asignaciones"]["fields"]["awr_asignaciones_awr_talleres_name"] = array (
  'name' => 'awr_asignaciones_awr_talleres_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AWR_ASIGNACIONES_AWR_TALLERES_FROM_AWR_TALLERES_TITLE',
  'save' => true,
  'id_name' => 'awr_asignaciones_awr_talleresawr_talleres_idb',
  'link' => 'awr_asignaciones_awr_talleres',
  'table' => 'awr_talleres',
  'module' => 'AWR_Talleres',
  'rname' => 'name',
);
$dictionary["AWR_Asignaciones"]["fields"]["awr_asignaciones_awr_talleresawr_talleres_idb"] = array (
  'name' => 'awr_asignaciones_awr_talleresawr_talleres_idb',
  'type' => 'link',
  'relationship' => 'awr_asignaciones_awr_talleres',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_AWR_ASIGNACIONES_AWR_TALLERES_FROM_AWR_TALLERES_TITLE',
);
