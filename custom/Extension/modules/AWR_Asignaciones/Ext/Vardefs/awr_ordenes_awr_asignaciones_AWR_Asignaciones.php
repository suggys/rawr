<?php
// created: 2019-03-10 05:21:40
$dictionary["AWR_Asignaciones"]["fields"]["awr_ordenes_awr_asignaciones"] = array (
  'name' => 'awr_ordenes_awr_asignaciones',
  'type' => 'link',
  'relationship' => 'awr_ordenes_awr_asignaciones',
  'source' => 'non-db',
  'module' => 'AWR_Ordenes',
  'bean_name' => 'AWR_Ordenes',
  'vname' => 'LBL_AWR_ORDENES_AWR_ASIGNACIONES_FROM_AWR_ORDENES_TITLE',
  'id_name' => 'awr_ordenes_awr_asignacionesawr_ordenes_ida',
);
$dictionary["AWR_Asignaciones"]["fields"]["awr_ordenes_awr_asignaciones_name"] = array (
  'name' => 'awr_ordenes_awr_asignaciones_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AWR_ORDENES_AWR_ASIGNACIONES_FROM_AWR_ORDENES_TITLE',
  'save' => true,
  'id_name' => 'awr_ordenes_awr_asignacionesawr_ordenes_ida',
  'link' => 'awr_ordenes_awr_asignaciones',
  'table' => 'awr_ordenes',
  'module' => 'AWR_Ordenes',
  'rname' => 'name',
);
$dictionary["AWR_Asignaciones"]["fields"]["awr_ordenes_awr_asignacionesawr_ordenes_ida"] = array (
  'name' => 'awr_ordenes_awr_asignacionesawr_ordenes_ida',
  'type' => 'link',
  'relationship' => 'awr_ordenes_awr_asignaciones',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_AWR_ORDENES_AWR_ASIGNACIONES_FROM_AWR_ORDENES_TITLE',
);
