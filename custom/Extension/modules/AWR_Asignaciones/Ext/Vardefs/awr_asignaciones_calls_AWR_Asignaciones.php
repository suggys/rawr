<?php
// created: 2019-03-10 05:21:38
$dictionary["AWR_Asignaciones"]["fields"]["awr_asignaciones_calls"] = array (
  'name' => 'awr_asignaciones_calls',
  'type' => 'link',
  'relationship' => 'awr_asignaciones_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'side' => 'right',
  'vname' => 'LBL_AWR_ASIGNACIONES_CALLS_FROM_CALLS_TITLE',
);
