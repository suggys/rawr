<?php
 // created: 2019-03-10 05:21:38
$layout_defs["AWR_Asignaciones"]["subpanel_setup"]['awr_asignaciones_calls'] = array (
  'order' => 100,
  'module' => 'Calls',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AWR_ASIGNACIONES_CALLS_FROM_CALLS_TITLE',
  'get_subpanel_data' => 'awr_asignaciones_calls',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
