<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

$app_list_strings['moduleList']['AWR_Clientes'] = 'Clientes';
$app_list_strings['moduleList']['AWR_Asignaciones'] = 'Asignaciones';
$app_list_strings['moduleList']['AWR_Imagenes'] = 'Imágenes';
$app_list_strings['moduleList']['AWR_Ordenes'] = 'Ordenes de Servicio';
$app_list_strings['moduleList']['AWR_Procesos'] = 'Procesos de Reparación';
$app_list_strings['moduleList']['AWR_Talleres'] = 'Talleres';
$app_list_strings['estatus_orden_list']['EsperaRecoleccion'] = 'Espera Recolección';
$app_list_strings['estatus_orden_list']['Recolectado'] = 'Recolectado';
$app_list_strings['estatus_orden_list']['RechazadoxTaller'] = 'Rechazado por Taller';
$app_list_strings['estatus_orden_list']['Taller'] = 'En Taller';
$app_list_strings['estatus_orden_list']['EnProceso'] = 'En Proceso';
$app_list_strings['estatus_orden_list']['Terminado'] = 'Terminado';
$app_list_strings['estatus_orden_list']['Entregado'] = 'Entregado';
$app_list_strings['estatus_orden_list']['Cancelado'] = 'Cancelado';
$app_list_strings['tipo_rin_list']['PendienteDefinir'] = 'Pendiente por Definir';
$app_list_strings['tipo_rin_list']['Recolectado'] = 'Recolectado';
$app_list_strings['tipo_rin_list']['RinPintado'] = 'Rin Pintado';
$app_list_strings['tipo_rin_list']['RinMaquinado'] = 'Rin Maquinado';
$app_list_strings['tipo_rin_list']['Sandblasteado'] = 'Acabado Sandblasteado';
$app_list_strings['tipo_rin_list']['RinFibreado'] = 'Rin Fibreado';
$app_list_strings['tipo_rin_list']['RinPulido'] = 'Rin Pulido';
$app_list_strings['tipo_rin_list']['Enderezado'] = 'Enderezado';
$app_list_strings['type_rin_list']['DesmontadoCarro'] = 'Desmontado de Carro';
$app_list_strings['type_rin_list']['DesmontadoRin'] = 'Desmontado de Rin';
$app_list_strings['type_rin_list']['Armado'] = 'Armado';
$app_list_strings['type_rin_list']['Lavado'] = 'Lavado';
$app_list_strings['type_rin_list']['Igualado'] = 'Igualado 1';
$app_list_strings['type_rin_list']['Igualado2'] = 'Igualado 2';
$app_list_strings['type_rin_list']['Quemado'] = 'Quemado';
$app_list_strings['type_rin_list']['Sandblast'] = 'Sandblast';
$app_list_strings['type_rin_list']['Enderezado'] = 'Enderezado';
$app_list_strings['type_rin_list']['Detallado'] = 'Detallado';
$app_list_strings['type_rin_list']['Soldado'] = 'Soldado';
$app_list_strings['type_rin_list']['MaquinadoCeja'] = 'Maquinado Ceja';
$app_list_strings['type_rin_list']['Pulido'] = 'Pulido';
$app_list_strings['type_rin_list']['Lavado2'] = 'Lavado 2';
$app_list_strings['type_rin_list']['Primer'] = 'Primer';
$app_list_strings['type_rin_list']['Lijado'] = 'Lijado';
$app_list_strings['type_rin_list']['Empapelado'] = 'Empapelado';
$app_list_strings['type_rin_list']['Pintado1'] = 'Pintado 1';
$app_list_strings['type_rin_list']['Pintado2'] = 'Pintado 2';
$app_list_strings['type_rin_list']['MaquinadoCara'] = 'Maquinado Cara';
$app_list_strings['type_rin_list']['Barniz'] = 'Barniz';
$app_list_strings['type_rin_list']['PulidoFinal'] = 'Pulido Final';
$app_list_strings['type_rin_list']['MontadoRin'] = 'Montado de Rin';
$app_list_strings['type_rin_list']['Preparado'] = 'Preparado';
$app_list_strings['type_rin_list']['MontadoCarro'] = 'Montado de Carro';
$app_list_strings['status_dom']['New'] = 'Nuevo';
$app_list_strings['status_dom']['Assigned'] = 'Asignado';
$app_list_strings['status_dom']['Closed'] = 'Cerrada';
$app_list_strings['status_dom']['PendingInput'] = 'Pendiente de información';
$app_list_strings['status_dom']['Rejected'] = 'Rechazada';
$app_list_strings['status_dom']['Duplicate'] = 'Duplicar';
$app_list_strings['recepcion_type_dom']['Administration'] = 'Administración';
$app_list_strings['recepcion_type_dom']['Product'] = 'Producto';
$app_list_strings['recepcion_type_dom']['User'] = 'Usuario';
$app_list_strings['estatus_reporte_list']['Pendiente'] = 'Pendiente';
$app_list_strings['estatus_reporte_list']['Confirmado'] = 'Confirmado';
$app_list_strings['estatus_reporte_list']['EnvioCorreo'] = 'Se envió correo';
$app_list_strings['estatus_reporte_list']['UnidadTransito'] = 'Unidad en Transito';
$app_list_strings['estatus_reporte_list']['PosiblePT'] = 'Posible PT';
$app_list_strings['estatus_reporte_list']['PosibleSustitucion'] = 'Posible Sustitución';
$app_list_strings['estatus_reporte_list']['PosiblePago'] = 'Posible pago de daños';
$app_list_strings['estatus_reporte_list']['PT'] = 'PT';
$app_list_strings['estatus_reporte_list']['Sustitucion'] = 'Sustitución';
$app_list_strings['estatus_reporte_list']['PagoDeDanios'] = 'Pago de Daños';
$app_list_strings['estatus_reporte_list']['CRPReparo'] = 'CRPReparo';
$app_list_strings['estatus_reporte_list']['ClienteNoReparacion'] = 'Cliente no requiere la reparación';
$app_list_strings['estatus_reporte_list']['NoPerteneceZona'] = 'No pertenece a la zona';
$app_list_strings['estatus_reporte_list']['FedexNoCuenta'] = 'Fedex no cuenta con cobertura';
$app_list_strings['estatus_reporte_list']['RinChino'] = 'Rin Chino';
$app_list_strings['estatus_reporte_list']['RinCromado'] = 'Rin Cromado';
$app_list_strings['estatus_reporte_list']['RinMalasReparacionesAnt'] = 'Rin con malas reparaciones anteriores';
$app_list_strings['estatus_final_list']['Pendiente'] = 'Pendiente';
$app_list_strings['estatus_final_list']['Reparado'] = 'Reparado';
$app_list_strings['estatus_final_list']['NoReparado'] = 'No Reparado';
$app_list_strings['estatus_final_list']['Cancelado'] = 'Cancelado';
$app_list_strings['tipo_cliente_list']['Aseguradora'] = 'Aseguradora';
$app_list_strings['tipo_cliente_list']['Particular'] = 'Particular';
