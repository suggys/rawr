<?php
// created: 2019-03-10 05:21:38
$dictionary["awr_asignaciones_awr_talleres"] = array (
  'true_relationship_type' => 'one-to-one',
  'relationships' => 
  array (
    'awr_asignaciones_awr_talleres' => 
    array (
      'lhs_module' => 'AWR_Asignaciones',
      'lhs_table' => 'awr_asignaciones',
      'lhs_key' => 'id',
      'rhs_module' => 'AWR_Talleres',
      'rhs_table' => 'awr_talleres',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'awr_asignaciones_awr_talleres_c',
      'join_key_lhs' => 'awr_asignaciones_awr_talleresawr_asignaciones_ida',
      'join_key_rhs' => 'awr_asignaciones_awr_talleresawr_talleres_idb',
    ),
  ),
  'table' => 'awr_asignaciones_awr_talleres_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'awr_asignaciones_awr_talleresawr_asignaciones_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'awr_asignaciones_awr_talleresawr_talleres_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'awr_asignaciones_awr_talleresspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'awr_asignaciones_awr_talleres_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'awr_asignaciones_awr_talleresawr_asignaciones_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'awr_asignaciones_awr_talleres_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'awr_asignaciones_awr_talleresawr_talleres_idb',
      ),
    ),
  ),
);