<?php
// created: 2019-03-10 05:21:38
$dictionary["awr_asignaciones_calls"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'awr_asignaciones_calls' => 
    array (
      'lhs_module' => 'AWR_Asignaciones',
      'lhs_table' => 'awr_asignaciones',
      'lhs_key' => 'id',
      'rhs_module' => 'Calls',
      'rhs_table' => 'calls',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'awr_asignaciones_calls_c',
      'join_key_lhs' => 'awr_asignaciones_callsawr_asignaciones_ida',
      'join_key_rhs' => 'awr_asignaciones_callscalls_idb',
    ),
  ),
  'table' => 'awr_asignaciones_calls_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'awr_asignaciones_callsawr_asignaciones_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'awr_asignaciones_callscalls_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'awr_asignaciones_callsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'awr_asignaciones_calls_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'awr_asignaciones_callsawr_asignaciones_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'awr_asignaciones_calls_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'awr_asignaciones_callscalls_idb',
      ),
    ),
  ),
);