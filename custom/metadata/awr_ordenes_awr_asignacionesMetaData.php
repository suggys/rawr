<?php
// created: 2019-03-10 05:21:39
$dictionary["awr_ordenes_awr_asignaciones"] = array (
  'true_relationship_type' => 'one-to-one',
  'relationships' => 
  array (
    'awr_ordenes_awr_asignaciones' => 
    array (
      'lhs_module' => 'AWR_Ordenes',
      'lhs_table' => 'awr_ordenes',
      'lhs_key' => 'id',
      'rhs_module' => 'AWR_Asignaciones',
      'rhs_table' => 'awr_asignaciones',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'awr_ordenes_awr_asignaciones_c',
      'join_key_lhs' => 'awr_ordenes_awr_asignacionesawr_ordenes_ida',
      'join_key_rhs' => 'awr_ordenes_awr_asignacionesawr_asignaciones_idb',
    ),
  ),
  'table' => 'awr_ordenes_awr_asignaciones_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'awr_ordenes_awr_asignacionesawr_ordenes_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'awr_ordenes_awr_asignacionesawr_asignaciones_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'awr_ordenes_awr_asignacionesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'awr_ordenes_awr_asignaciones_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'awr_ordenes_awr_asignacionesawr_ordenes_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'awr_ordenes_awr_asignaciones_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'awr_ordenes_awr_asignacionesawr_asignaciones_idb',
      ),
    ),
  ),
);