<?php
// created: 2019-03-08 20:09:42
$GLOBALS['tabStructure'] = array (
  'LBL_TABGROUP_SALES' => 
  array (
    'label' => 'LBL_TABGROUP_SALES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'AWR_Asignaciones',
      3 => 'AWR_Imagenes',
      4 => 'AWR_Ordenes',
      5 => 'AWR_Procesos',
      6 => 'AWR_Talleres',
    ),
  ),
);