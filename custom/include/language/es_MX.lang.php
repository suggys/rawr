<?php
$app_strings['LBL_TABGROUP_SALES'] = 'AWR';
$app_list_strings['moduleList']['Accounts']='Clientes';
$app_list_strings['moduleListSingular']['Accounts']='Cliente';
$app_list_strings['record_type_display']['Accounts']='Cliente';
$app_list_strings['parent_type_display']['Accounts']='Cliente';
$app_list_strings['record_type_display_notes']['Accounts']='Cliente';
$GLOBALS['app_list_strings']['pdf_template_type_dom']=array (
  'AOS_Quotes' => 'Cotizaciones',
  'AOS_Invoices' => 'Facturas',
  'AOS_Contracts' => 'Contratos',
  'Accounts' => 'Cuentas',
  'Contacts' => 'Contactos',
  'Leads' => 'Clientes potenciales',
  'AWR_Ordenes' => 'Ordenes de servicio',
);